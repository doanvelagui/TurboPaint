// Interface Generator

#include <sys/nearptr.h>
#include <stdio.h>
#include "video.h"
#include "bitmap.h"
#include "glib.h"

void fondo (int x1, int y1, int x2, int y2){
	draw_Rectangle(x1, y1, x2, y2, 27, 1);
	draw_Rectangle(x1 + 1, y1 + 1, x2 - 1, y2 - 1, 16, 1);
	fill_Rectangle(x1 + 1, y1 + 1, x2 - 1, y2 - 1, 26);
}

// Generating paint interface

void generate_principal_menu_bar(){
	int i;
	int x1 = 85, x2 = 110, y = 55;
	fill_Rectangle(x1 - 5, y - 5, 319, y + 20, 28);
	draw_Rectangle(x1 - 5, y - 5, 319, y + 20, 24, 1);
	
	x1 += 5;
	x2 += 5;
	fondo (x1, y, x1 + 20, y + 15); // new
	draw_Rectangle(x1 + 7, y + 3, x1 + 13, y + 11, 30, 1);
	fill_Rectangle(x1 + 7, y + 3, x1 + 13, y + 11, 15);
	
	
	fondo (x2, y, x2 + 20, y + 15); // open
	fill_Rectangle(x2 + 4, y + 5, x2 + 16, y + 12, 14);
	fill_Rectangle(x2 + 10, y + 3, x2 + 14, y + 4, 14);
	
	x1 += 50;
	x2 += 50;
	
	fondo (x1, y, x1 + 20, y + 15); // save
	fill_Rectangle(x1 + 5, y + 3, x1 + 15, y + 12, 8);
	fill_Rectangle(x1 + 7, y + 7, x1 + 13, y + 10, 15);
	fill_Rectangle(x1 + 7, y + 3, x1 + 13, y + 6, 17);
	
	fondo (x2, y, x2 + 20, y + 15); // undo
	draw_Line(x2 + 5, y + 7, x2 + 15, y + 7, 1, 3);
	for (i = 4; i < 10; i++){
		draw_Line(x2 + i, y + 3 + i, x2 + i, y + 10 - i, 1, 1);
	}
	
	x1 += 50;
	x2 += 50;
	
	fondo (x1, y, x1 + 20, y + 15); // redo
	draw_Line(x1 + 4, y + 7, x1 + 14, y + 7, 1, 3);
	int j = 1;
	for (i = 15; i > 9; i--){
		draw_Line(x1 + i, y + 6 + j, x1 + i, y + 7 - j, 1, 1);
		j ++;
	}
	
	fondo (x2, y, x2 + 20, y + 15); // cut
	draw_Line(x2 + 5, y + 2, x2 + 12, y + 9, 8, 1);
	draw_Line(x2 + 8, y + 8, x2 + 15, y + 1, 8, 1);
	draw_Circle(x2 + 6, y + 10, 2, 1, 1);
	draw_Circle(x2 + 13, y + 10, 2, 1, 1);
	
	x1 += 50;
	x2 += 50;
	
	fondo (x1, y, x1 + 20, y + 15); // copy
	fill_Rectangle(x1 + 8, y + 2, x1 + 13, y + 10, 15);
	draw_Rectangle(x1 + 8, y + 2, x1 + 13, y + 10, 8, 1);
	fill_Rectangle(x1 + 6, y + 5, x1 + 11, y + 12, 15);
	draw_Rectangle(x1 + 6, y + 5, x1 + 11, y + 12, 8, 1);
	
	x1 += 25;
	x2 += 25;
	
	fondo (x1, y, x1 + 20, y + 15); // paste
	draw_Rectangle(x1 + 6, y + 2, x1 + 13, y + 11, 6, 1);
	fill_Rectangle(x1 + 7, y + 3, x1 + 13, y + 11, 15);
	draw_Line(x1 + 8, y + 3, x1 + 11, y + 3, 8, 2);
	
	fondo (x2, y, x2 + 20, y + 15); //close
	draw_Line(x2 + 5, y + 3, x2 + 15, y + 13, 4, 1);
	draw_Line(x2 + 5, y + 12, x2 + 15, y + 2, 4, 1);
	
	save_bitmap_from(VGA, "pbar.bmp", 80, 50, 320, 75);
}

void generate_tools_bar(){
	int i;
	int x1 = 105, x2 = 130, x3 = 155, y = 55;
	fill_Rectangle(x1 - 5, y - 5, x3 + 25, y + 80, 28);
	draw_Rectangle(x1 - 5, y - 5, x3 + 24, y + 80, 24, 1);
	
	fondo (x1, y, x1 + 20, y + 15); // select
	for (i = 3; i; i--){
		put_pixel(x1 + 5*i - 2, y + 3, 15);
		put_pixel(x1 + 5*i - 1, y + 3, 15);
		put_pixel(x1 + 5*i, y + 3, 15);
		put_pixel(x1 + 5*i - 2, y + 10, 15);
		put_pixel(x1 + 5*i - 1, y + 10, 15);
		put_pixel(x1 + 5*i, y + 10, 15);
	}
	for (i = 2; i; i--){
		put_pixel(x1 + 3, y + 4*i, 15);
		put_pixel(x1 + 3, y + 4*i + 1, 15);
		put_pixel(x1 + 15, y + 4*i, 15);
		put_pixel(x1 + 15, y + 4*i + 1, 15);
	}
	
	fondo (x2, y, x2 + 20, y + 15); // line
	draw_Line(x2 + 5, y + 12, x2 + 15, y + 2, 1, 1);
	
	fondo (x3, y, x3 + 20, y + 15); // rectangle
	draw_Rectangle(x3 + 5, y + 11, x3 + 15, y + 4, 1, 1);
	
	y += 20;
	
	fondo (x1, y, x1 + 20, y + 15); // circle
	draw_Circle(x1 + 10, y + 7, 5, 1, 1);
	
	fondo (x2, y, x2 + 20, y + 15); // ellipse
	draw_Ellipse(x2 + 9, y + 7, 6, 4, 1, 1);
	
	fondo (x3, y, x3 + 20, y + 15); // polygon
	int arrx[4] = {x3 + 7, x3 + 13, x3 + 16, x3 + 4};
	int arry[4] = {y + 3, y + 3, y + 11, y + 11};
	draw_Polygon(4, arrx, arry, 1, 1);
	
	y += 20;
	
	fondo (x1, y, x1 + 20, y + 15); // spray
	for (i = 20; i; i--){
		put_pixel(rand() % 14 + x1 + 3, rand() % 9 + y + 3, 1);
	}
	
	fondo (x2, y, x2 + 20, y + 15); // marker
	draw_Line(x2 + 5, y + 12, x2 + 15, y + 12, 1, 1);
	draw_Line(x2 + 6, y + 11, x2 + 15, y + 2, 16, 3);
	
	fondo (x3, y, x3 + 20, y + 15); // eraser
	fill_Rectangle(x3 + 5, y + 5, x3 + 13, y + 12, 15);
	fill_Rectangle(x3 + 6, y + 4, x3 + 14, y + 11, 15);
	fill_Rectangle(x3 + 7, y + 3, x3 + 15, y + 10, 15);
	
	y += 20;
	
	fondo (x1, y, x1 + 20, y + 15); // fill
	fill_Ellipse(x1 + 13, y + 10, 5, 2, 1);
	draw_Ellipse(x1 + 13, y + 10, 5, 2, 8, 1);
	for (i = 0; i<5; i++){
		draw_Line(x1 + 3 + i, y + 6 + i,x1 + 8 + i, y + 1 + i, 20, 1);
		draw_Line(x1 + 3 + i, y + 5 + i,x1 + 8 + i, y + i, 20, 1);
	}
	
	fondo (x2, y, x2 + 20, y + 15); // get colour
	fill_Ellipse(x2 + 7, y + 10, 5, 2, 1);
	draw_Ellipse(x2 + 7, y + 10, 5, 2, 8, 1);
	draw_Line(x2 + 6, y + 10, x2 + 7, y + 11, 7, 1);
	draw_Line(x2 + 7, y + 9, x2 + 11, y + 5, 20, 3);
	fill_Circle(x2 + 12, y + 4, 3, 20);
	
	// fondo (x3, y, x3 + 20, y + 15); // texto
	
	save_bitmap_from(VGA, "tbar.bmp", x1 - 5, 50, x3 + 25, 135);
}

void generate_empty_options(){
	int x = 100;
	fill_Rectangle(x, 85, x + 80, 130, 28);
	draw_Rectangle(x, 85, x + 79, 130, 24, 1);
	save_bitmap_from(VGA, "empty.bmp", x, 85, x + 80, 130);
}

void generate_line_options(){
	int x1 = 105, x2 = 130, x3 = 155, y = 90;
	fill_Rectangle(x1 - 5, y - 5, x3 + 25, y + 40, 28);
	draw_Rectangle(x1 - 5, y - 5, x3 + 24, y + 40, 24, 1);
	
	fondo (x1, y, x1 + 20, y + 15); // thick = 1
	draw_Line(x1 + 5, y + 12, x1 + 15, y + 2, 1, 1);
	
	fondo (x2, y, x2 + 20, y + 15); // thick = 2
	draw_Line(x2 + 6, y + 11, x2 + 15, y + 2, 1, 2);
	
	fondo (x3, y, x3 + 20, y + 15); // thick = 3
	draw_Line(x3 + 6, y + 11, x3 + 15, y + 2, 1, 3);
	
	save_bitmap_from(VGA, "tline.bmp", 100, 85, 180, 130);
}

void generate_rectangle_options(){
	int x1 = 105, x2 = 130, x3 = 155, y = 90;
	fill_Rectangle(x1 - 5, y - 5, x3 + 25, y + 40, 28);
	draw_Rectangle(x1 - 5, y - 5, x3 + 24, y + 40, 24, 1);
	
	fondo (x1, y, x1 + 20, y + 15); // without fill
	draw_Rectangle(x1 + 5, y + 10, x1 + 15, y + 3, 1, 1);
	
	fondo (x2, y, x2 + 20, y + 15); // with thick = 2
	draw_Rectangle(x2 + 5, y + 10, x2 + 15, y + 3, 1, 2);
	
	fondo (x3, y, x3 + 20, y + 15); // with thick = 3
	draw_Rectangle(x3 + 5, y + 10, x3 + 15, y + 4, 1, 3);
	
	y += 20;
	
	fondo (x1, y, x1 + 20, y + 15); // with fill
	fill_Rectangle(x1 + 5, y + 10, x1 + 15, y + 3, 1);
	
	fondo (x2, y, x2 + 20, y + 15); // with fill and draw
	fill_Rectangle(x2 + 5, y + 10, x2 + 15, y + 3, 2);
	draw_Rectangle(x2 + 5, y + 10, x2 + 15, y + 3, 1, 1);
	
	save_bitmap_from(VGA, "trect.bmp", 100, 85, 180, 130);
}

void generate_circle_options(){
	int x1 = 105, x2 = 130, x3 = 155, y = 90;
	fill_Rectangle(x1 - 5, y - 5, x3 + 25, y + 40, 28);
	draw_Rectangle(x1 - 5, y - 5, x3 + 24, y + 40, 24, 1);
	
	fondo (x1, y, x1 + 20, y + 15); // without fill
	draw_Circle(x1 + 10, y + 7, 5, 1, 1);
	
	fondo (x2, y, x2 + 20, y + 15); // with thick = 2
	draw_Circle(x2 + 9, y + 7, 4, 1, 2);
	
	fondo (x3, y, x3 + 20, y + 15); // with thick = 3
	draw_Circle(x3 + 9, y + 7, 4, 1, 3);
	
	y += 20;
	
	fondo (x1, y, x1 + 20, y + 15); // with fill
	fill_Circle(x1 + 10, y + 7, 5, 1);
	
	fondo (x2, y, x2 + 20, y + 15); // with fill and draw
	fill_Circle(x2 + 10, y + 7, 5, 2);
	draw_Circle(x2 + 10, y + 7, 5, 1, 1);
	
	save_bitmap_from(VGA, "tcirc.bmp", 100, 85, 180, 130);
}

void generate_spray_options(){
	int i;
	int x1 = 105, x2 = 130, x3 = 155, y = 90;
	fill_Rectangle(x1 - 5, y - 5, x3 + 25, y + 40, 28);
	draw_Rectangle(x1 - 5, y - 5, x3 + 24, y + 40, 24, 1);
	
	fondo (x1, y, x1 + 20, y + 15); // little
	for (i = 8; i; i--){
		put_pixel(rand() % 6 + x1 + 7, rand() % 6 + y + 4, 1);
	}
	
	fondo (x2, y, x2 + 20, y + 15); // medium
	for (i = 20; i; i--){
		put_pixel(rand() % 10 + x2 + 5, rand() % 9 + y + 2, 1);
	}
	
	fondo (x3, y, x3 + 20, y + 15); // big
	for (i = 30; i; i--){
		put_pixel(rand() % 14 + x3 + 3, rand() % 12 + y + 1, 1);
	}
	
	save_bitmap_from(VGA, "tspra.bmp", 100, 85, 180, 130);
}

void generate_fill_options(){
	int x1 = 105, x2 = 130, x3 = 155, y = 90;
	fill_Rectangle(x1 - 5, y - 5, x3 + 25, y + 40, 28);
	draw_Rectangle(x1 - 5, y - 5, x3 + 24, y + 40, 24, 1);
	
	fondo (x1, y, x1 + 20, y + 15); // solid
	draw_Rectangle(x1 + 5, y + 3, x1+ 15, y + 12, 16, 1);
	solid_Fill(x1 + 10, y + 8, 1, 26);
	
	fondo (x2, y, x2 + 20, y + 15); // pattern 1
	draw_Rectangle(x2 + 5, y + 3, x2+ 15, y + 12, 16, 1);
	pattern1_Fill(x2 + 10, y + 8, 1, 2, 26, 0);
	
	fondo (x3, y, x3 + 20, y + 15); // pattern 2
	draw_Rectangle(x3 + 5, y + 3, x3+ 15, y + 12, 16, 1);
	pattern2_Fill(x3 + 10, y + 7, 1, 2, 26, 0, 0);
	
	// TODO add more patterns
	
	save_bitmap_from(VGA, "tfill.bmp", 100, 85, 180, 130);
}

void generate_text_options(){
	// TODO
}

void generate_button_effect(){
	int x1 = 100, y1 = 100;
	fill_Rectangle(90, 90, 130, 130, 0);
	draw_H_Line(x1, x1 + 20, y1 + 14, 26, 1);
	draw_V_Line(x1 + 19, y1, y1 + 14, 26, 1);
	draw_Rectangle(x1, y1, x1 + 20, y1 + 15, 9, 1);
	save_bitmap_from(VGA, "press.bmp", 100, 100, 121, 116);
	
	draw_H_Line(x1, x1 + 20, y1 + 14, 16, 1);
	draw_V_Line(x1 + 19, y1, y1 + 14, 16, 1);
	draw_Rectangle(x1, y1, x1 + 20, y1 + 15, 27, 1);
	save_bitmap_from(VGA, "release.bmp", 100, 100, 121, 116);
}

void generate_palette_color(){
	int i;
	fill_Rectangle(100, 130, 180, 199, 28);
	draw_Rectangle(100, 130, 179, 199, 24, 1);
	
	fill_Rectangle(110, 132, 135, 146, 16);
	fill_Rectangle(145, 132, 170, 146, 15);
	draw_Rectangle(110, 132, 135, 146, 16, 1);
	draw_Rectangle(145, 132, 170, 146, 16, 1);
	
	draw_Rectangle(103, 148, 176, 197, 16, 1);
	
	int bc[12] = {1, 5, 4, 6, 2, 3, 9, 13, 12, 14, 10, 11};
	int x = 104, y = 149, c;
	for (i = 0; i<12; i++){
		if (x > 175){
			y += 4;
			x = 104;
		}
		fill_Rectangle(x, y, x + 12, y + 4, bc[i]);
		x += 12;
	}
	x = 104; y = 157; c = 32;
	for (i = 216; i; i--){
		if (x > 175){
			y += 4;
			x = 104;
		}
		fill_Rectangle(x, y, x + 3, y + 4, c);
		x += 3;
		c ++;
	}
	x = 104; y += 4; c = 16;
	for (i = 16; i; i--){
		fill_Rectangle(x, y, x + 5, y + 4, c);
		c ++;
		if (c % 2)
			x += 4;
		else
			x += 5;
	}
	
	save_bitmap_from(VGA, "palette.bmp", 100, 130, 180, 200);
}

void generate_pointer(){
	int i;
	fill_Rectangle(90, 90, 130, 130, 0);
	draw_Line(100, 101, 100, 115, 15, 1);
	draw_Line(100, 100, 110, 110, 28, 1);
	draw_Line(107, 110, 111, 110, 28, 1);
	draw_Line(101, 113, 104, 110, 28, 1);
	
	for (i=4; i; i--){
		put_pixel(105+i, 108+2*i, 28);
		put_pixel(105+i, 109+2*i, 28);
	}
	for (i=3; i; i--){
		put_pixel(103+i, 110+2*i, 28);
		put_pixel(103+i, 111+2*i, 28);
	}
	draw_Line(107, 118, 109, 118, 28, 1);
	solid_Fill(104, 108, 18, 0);
	
	save_bitmap_from(VGA, "pointer.bmp", 100, 100, 112, 120);
}

// Generate transformations interface

void r_arrow(int x, int y, int b){
	put_pixel(x+2, y, 1);
	put_pixel(x+2, y+1, 1);
	put_pixel(x+3, y+1, 1);
	put_pixel(x+2, y+2, 1);
	put_pixel(x+3, y+2, 1);
	put_pixel(x+4, y+2, 1);
	put_pixel(x+2, y+3, 1);
	put_pixel(x+3, y+3, 1);
	put_pixel(x+2, y+4, 1);
	
	if (b){
		put_pixel(x, y+2, 1);
		put_pixel(x+1, y+2, 1);
	}
}

void l_arrow(int x, int y, int b){
	put_pixel(x+2, y, 1);
	put_pixel(x+2, y+1, 1);
	put_pixel(x+1, y+1, 1);
	put_pixel(x+2, y+2, 1);
	put_pixel(x+1, y+2, 1);
	put_pixel(x,   y+2, 1);
	put_pixel(x+2, y+3, 1);
	put_pixel(x+1, y+3, 1);
	put_pixel(x+2, y+4, 1);
	
	if (b){
		put_pixel(x+3, y+2, 1);
		put_pixel(x+4, y+2, 1);
	}
}

void u_arrow(int x, int y, int b){
	put_pixel(x+2, y, 1);
	put_pixel(x+1, y+1, 1);
	put_pixel(x+2, y+1, 1);
	put_pixel(x+3, y+1, 1);
	put_pixel(x,   y+2, 1);
	put_pixel(x+1, y+2, 1);
	put_pixel(x+2, y+2, 1);
	put_pixel(x+3, y+2, 1);
	put_pixel(x+4, y+2, 1);
	
	if (b){
		put_pixel(x+2, y+3, 1);
		put_pixel(x+2, y+4, 1);
	}
}

void d_arrow(int x, int y, int b){
	put_pixel(x+2, y+4, 1);
	put_pixel(x+1, y+3, 1);
	put_pixel(x+2, y+3, 1);
	put_pixel(x+3, y+3, 1);
	put_pixel(x,   y+2, 1);
	put_pixel(x+1, y+2, 1);
	put_pixel(x+2, y+2, 1);
	put_pixel(x+3, y+2, 1);
	put_pixel(x+4, y+2, 1);
	
	if (b){
		put_pixel(x+2, y,   1);
		put_pixel(x+2, y+1, 1);
	}
}

void generate_pivot(){
	fill_Rectangle(100, 100, 112, 122, 0);
	draw_H_Line(100, 111, 105, 15, 1);
	draw_V_Line(105, 100, 111, 15, 1);
	draw_Circle(105, 105, 3, 15, 1);
	save_bitmap_from(VGA, "pivot.bmp", 100, 100, 112, 120);
}

void generate_exit_button(){
	int x = 100, y = 100;
	fill_Rectangle(x, y, x + 55, y + 25, 28);
	draw_Rectangle(x, y, x + 54, y + 24, 24, 1);
	
	x += 5; y += 5;
	// TODO help
	
	x += 25;
	fondo(x, y, x + 20, y + 15); //close
	draw_Line(x + 5, y + 3, x + 15, y + 13, 4, 1);
	draw_Line(x + 5, y + 12, x + 15, y + 2, 4, 1);
	
	save_bitmap_from(VGA, "tpbar.bmp", 100, 100, 155, 125);
}

void generate_object_selection(){
	int x = 100, y = 100;
	fill_Rectangle(x, y, x + 55, y + 25, 28);
	draw_Rectangle(x, y, x + 54, y + 24, 24, 1);
	
	x += 5; y += 5;
	fondo(x, y, x + 20, y + 15); //house
	fill_Rectangle(x + 7, y + 7, x + 14, y + 12, 9);
	int px[3] = {x + 5, x + 15, x + 10};
	int py[3] = {y + 7, y + 7, y + 3};
	fill_Polygon(3, px, py, 115);
	
	x += 25;
	fondo(x, y, x + 20, y + 15); //tree
	fill_Rectangle(x + 9, y + 7, x + 12, y + 12, 6);
	int pxx[3] = {x + 6, x + 14, x + 10};
	int pyy[3] = {y + 8, y + 8, y + 2};
	fill_Polygon(3, pxx, pyy, 10);
	
	save_bitmap_from(VGA, "tsele.bmp", 100, 100, 155, 125);
}

void generate_transformation_selection(){
	int x = 100, y = 100;
	fill_Rectangle(x, y, x + 55, y + 65, 28);
	draw_Rectangle(x, y, x + 54, y + 64, 24, 1);
	
	x += 5; y += 5;
	fondo(x, y, x + 20, y + 15); //translation
	u_arrow(x + 8, y + 2, 1);
	d_arrow(x + 8, y + 8, 1);
	l_arrow(x + 4, y + 5, 1);
	r_arrow(x + 12, y + 5, 1);
	draw_H_Line(x + 9, x + 12, y + 7, 1, 1);
	
	x += 25;
	fondo(x, y, x + 20, y + 15); //escalation
	draw_Circle(x + 8, y + 6, 4, 1, 1);
	draw_H_Line(x + 6, x + 11, y + 6, 1, 1);
	draw_V_Line(x + 8, y + 4, y + 9, 1, 1);
	draw_Line(x + 11, y + 9, x + 14, y + 12, 1, 2);
	
	x -= 25; y += 20;
	fondo(x, y, x + 20, y + 15); //rotation
	draw_Circle(x + 10, y + 8, 4, 1, 1);
	fill_Rectangle(x + 11, y + 1, x + 18, y + 8, 26);
	r_arrow(x + 8, y + 2, 0);
	
	x += 25;
	fondo(x, y, x + 20, y + 15); //shear
	int i;
	for (i = 2; i; i--){
		put_pixel(x + 5*i - 2, y + 3, 15);
		put_pixel(x + 5*i - 1, y + 3, 15);
		put_pixel(x + 5*i, y + 3, 15);
		put_pixel(x + 5*i - 2, y + 10, 15);
		put_pixel(x + 5*i - 1, y + 10, 15);
		put_pixel(x + 5*i, y + 10, 15);
	}
	for (i = 2; i; i--){
		put_pixel(x + 3, y + 4*i, 15);
		put_pixel(x + 3, y + 4*i + 1, 15);
		put_pixel(x + 10, y + 4*i, 15);
		put_pixel(x + 10, y + 4*i + 1, 15);
	}
	int px[4] = {x + 3, x + 10, x + 17, x + 10};
	int py[4] = {y + 10, y + 10, y + 3, y + 3};
	draw_Polygon(4, px, py, 1, 1);
	
	x -= 25; y += 20;
	fondo(x, y, x + 20, y + 15); //flip
	draw_V_Line(x + 9, y + 1, y + 4, 15, 1);
	draw_V_Line(x + 9, y + 6, y + 9, 15, 1);
	draw_V_Line(x + 9, y + 11, y + 14, 15, 1);
	int px1[3] = {x + 3, x + 7, x + 7};
	int px2[3] = {x + 15, x + 11, x + 11};
	int pyy[3] = {y + 11, y + 11, y + 2};
	draw_Polygon(3, px1, pyy, 1, 1);
	draw_Polygon(3, px2, pyy, 1, 1);
	
	save_bitmap_from(VGA, "ttool.bmp", 100, 100, 155, 165);
}

void generate_translate_tools(){
	int x = 100, y = 100;
	fill_Rectangle(x, y, x + 55, y + 45, 28);
	draw_Rectangle(x, y, x + 54, y + 44, 24, 1);
	
	x += 5; y += 5;
	fondo(x, y, x + 20, y + 15); //up
	u_arrow(x + 7, y + 4, 1);
	u_arrow(x + 7, y + 5, 1);
	u_arrow(x + 6, y + 5, 1);
	u_arrow(x + 8, y + 5, 1);
	
	x += 25;
	fondo(x, y, x + 20, y + 15); //down
	d_arrow(x + 7, y + 6, 1);
	d_arrow(x + 7, y + 5, 1);
	d_arrow(x + 6, y + 5, 1);
	d_arrow(x + 8, y + 5, 1);
	
	x -= 25; y += 20;
	fondo(x, y, x + 20, y + 15); //left
	l_arrow(x + 7, y + 5, 1);
	l_arrow(x + 8, y + 5, 1);
	l_arrow(x + 8, y + 4, 1);
	l_arrow(x + 8, y + 6, 1);
	
	x += 25;
	fondo(x, y, x + 20, y + 15); //right
	r_arrow(x + 9, y + 5, 1);
	r_arrow(x + 8, y + 5, 1);
	r_arrow(x + 8, y + 4, 1);
	r_arrow(x + 8, y + 6, 1);
	
	save_bitmap_from(VGA, "ttran.bmp", 100, 100, 155, 145);
}

void generate_escalation_tools(){
	int x = 100, y = 100;
	fill_Rectangle(x, y, x + 55, y + 45, 28);
	draw_Rectangle(x, y, x + 54, y + 44, 24, 1);
	
	x += 5; y += 5;
	fondo(x, y, x + 20, y + 15); //x scale positive
	l_arrow(x + 4, y + 5, 1);
	r_arrow(x + 12, y + 5, 1);
	
	x += 25;
	fondo(x, y, x + 20, y + 15); //x scale negative
	r_arrow(x + 4, y + 5, 1);
	l_arrow(x + 12, y + 5, 1);
	
	x -= 25; y += 20;
	fondo(x, y, x + 20, y + 15); //y scale positive
	u_arrow(x + 8, y + 2, 1);
	d_arrow(x + 8, y + 8, 1);
	
	
	x += 25;
	fondo(x, y, x + 20, y + 15); //y scale negative
	d_arrow(x + 8, y + 2, 1);
	u_arrow(x + 8, y + 8, 1);
	
	save_bitmap_from(VGA, "tscal.bmp", 100, 100, 155, 145);
}

void generate_rotation_tools(){
	int x = 100, y = 100;
	fill_Rectangle(x, y, x + 55, y + 45, 28);
	draw_Rectangle(x, y, x + 54, y + 44, 24, 1);
	
	x += 5; y += 5;
	fondo(x, y, x + 20, y + 15); //clock side
	draw_Circle(x + 10, y + 8, 4, 1, 1);
	fill_Rectangle(x + 11, y + 1, x + 18, y + 8, 26);
	r_arrow(x + 8, y + 2, 0);
	
	x += 25;
	fondo(x, y, x + 20, y + 15); //anti clock side
	draw_Circle(x + 10, y + 8, 4, 1, 1);
	fill_Rectangle(x + 3, y + 1, x + 10, y + 8, 26);
	l_arrow(x + 8, y + 2, 0);
	
	save_bitmap_from(VGA, "trota.bmp", 100, 100, 155, 145);
}

void generate_shear_tools(){
	int x = 100, y = 100;
	fill_Rectangle(x, y, x + 55, y + 45, 28);
	draw_Rectangle(x, y, x + 54, y + 44, 24, 1);
	
	x += 5; y += 5;
	fondo(x, y, x + 20, y + 15); //x positive shear
	int xpx[4] = {x + 3, x + 10, x + 17, x + 10};
	int xpy[4] = {y + 10, y + 10, y + 3, y + 3};
	draw_Polygon(4, xpx, xpy, 1, 1);
	
	x += 25;
	fondo(x, y, x + 20, y + 15); //x negative shear
	int xnx[4] = {x + 17, x + 10, x + 3, x + 10};
	int xny[4] = {y + 10, y + 10, y + 3, y + 3};
	draw_Polygon(4, xnx, xny, 1, 1);
	
	x -= 25; y += 20;
	fondo(x, y, x + 20, y + 15); //y positive shear
	int ypx[4] = {x + 5, x + 5, x + 15, x + 15};
	int ypy[4] = {y + 6, y + 12, y + 8, y + 2};
	draw_Polygon(4, ypx, ypy, 1, 1);
	
	x += 25;
	fondo(x, y, x + 20, y + 15); //y negative shear
	int ynx[4] = {x + 5, x + 5, x + 15, x + 15};
	int yny[4] = {y + 8, y + 2, y + 6, y + 12};
	draw_Polygon(4, ynx, yny, 1, 1);
	
	save_bitmap_from(VGA, "tshea.bmp", 100, 100, 155, 145);
}

void generate_flip_tools(){
	int x = 100, y = 100;
	fill_Rectangle(x, y, x + 55, y + 45, 28);
	draw_Rectangle(x, y, x + 54, y + 44, 24, 1);
	
	x += 5; y += 5;
	fondo(x, y, x + 20, y + 15); // x flip
	draw_H_Line(x + 1, x + 5, y + 7, 15, 1);
	draw_H_Line(x + 7, x + 11, y + 7, 15, 1);
	draw_H_Line(x + 14, x + 18, y + 7, 15, 1);
	int pxx[3] = {x + 5, x + 5, x + 15};
	int py1[3] = {y + 5, y + 2, y + 5};
	int py2[3] = {y + 12, y + 9, y + 9};
	draw_Polygon(3, pxx, py1, 1, 1);
	draw_Polygon(3, pxx, py2, 1, 1);
	
	x += 25;
	fondo(x, y, x + 20, y + 15); // y flip
	draw_V_Line(x + 9, y + 1, y + 4, 15, 1);
	draw_V_Line(x + 9, y + 6, y + 9, 15, 1);
	draw_V_Line(x + 9, y + 11, y + 14, 15, 1);
	int px1[3] = {x + 3, x + 7, x + 7};
	int px2[3] = {x + 15, x + 11, x + 11};
	int pyy[3] = {y + 11, y + 11, y + 2};
	draw_Polygon(3, px1, pyy, 1, 1);
	draw_Polygon(3, px2, pyy, 1, 1);
	
	save_bitmap_from(VGA, "tflip.bmp", 100, 100, 155, 145);
}

void generate_free_space(){
	int x = 100, y = 100;
	fill_Rectangle(100, 100, 155, 140, 28);
	draw_Rectangle(100, 100, 154, 139, 24, 1);
	save_bitmap_from(VGA, "tfree.bmp", 100, 100, 155, 140);
}

// Main method

int main(){
    set_VGA_256();
    
	//*/ Generate paint interface
	generate_principal_menu_bar();
	generate_tools_bar();
	generate_empty_options();
	generate_line_options();
	generate_rectangle_options();
	generate_circle_options();
	generate_spray_options();
	generate_fill_options();
	
	//generate_text_options(); // TODO
	
	generate_button_effect();
	generate_pointer();
	generate_palette_color();
	//*/
	
	/*/ Generate trans interface
	generate_exit_button();
	generate_object_selection();
	generate_transformation_selection();
	generate_translate_tools();
	generate_escalation_tools();
	generate_rotation_tools();
	generate_shear_tools();
	generate_flip_tools();
	generate_free_space();
	//generate_pivot();
	generate_button_effect();
	generate_pointer();
	//*/
	
	set_TEXT();
	return 0;
}
