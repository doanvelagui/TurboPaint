#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <sys/nearptr.h>

#include "mouse.h"

void get_mouse_motion(sword *dx, sword *dy){
	union REGS regs;
	regs.x.ax = MOUSE_GETMOTION;
	int86(MOUSE_INT, &regs, &regs);
	*dx=regs.x.cx;
	*dy=regs.x.dx;
}

sword init_mouse(MOUSE *mouse){
	sword dx,dy;
	union REGS regs;
	regs.x.ax = MOUSE_RESET;
	int86(MOUSE_INT, &regs, &regs);
	mouse->on=regs.x.ax;
	mouse->num_buttons=regs.x.bx;
	mouse->button1=0;
	mouse->button2=0;
	mouse->button3=0;
	mouse->x=SCREEN_WIDTH/2;
	mouse->y=SCREEN_HEIGHT/2;
	get_mouse_motion(&dx,&dy);
	return mouse->on;
}

sword get_mouse_press(sword button){
	union REGS regs;
	regs.x.ax = MOUSE_GETPRESS;
	regs.x.bx = button;
	int86(MOUSE_INT, &regs, &regs);
	return regs.x.bx;
}

sword get_mouse_release(sword button){
	union REGS regs;
	regs.x.ax = MOUSE_GETRELEASE;
	regs.x.bx = button;
	int86(MOUSE_INT, &regs, &regs);
	return regs.x.bx;
}

void show_mouse(MOUSE *mouse, byte *VGA){
	int x, y;
	int mx = mouse->x - mouse->bmp->hot_x;
	int my = mouse->y - mouse->bmp->hot_y;
	long screen_offset = (my<<8)+(my<<6);
	word bitmap_offset = 0;
	byte data;

	for(y=0;y<MOUSE_HEIGHT;y++){
		for(x=0;x<MOUSE_WIDTH;x++,bitmap_offset++){
			mouse->under[bitmap_offset] = VGA[(word)(screen_offset+mx+x)];
			if (mx+x < SCREEN_WIDTH  && mx+x >= 0 && my+y < SCREEN_HEIGHT && my+y >= 0){
				data = mouse->bmp->data[bitmap_offset];
				if (data) VGA[(word)(screen_offset+mx+x)] = data;
			}
		}
		screen_offset+=SCREEN_WIDTH;
	}
}

void hide_mouse(MOUSE *mouse, byte *VGA){
	int x, y;
	int mx = mouse->x - mouse->bmp->hot_x;
	int my = mouse->y - mouse->bmp->hot_y;
	long screen_offset = (my<<8)+(my<<6);
	word bitmap_offset = 0;

	for(y=0;y<MOUSE_HEIGHT;y++){
		for(x=0;x<MOUSE_WIDTH;x++,bitmap_offset++){
			if (mx+x < SCREEN_WIDTH  && mx+x >= 0 && my+y < SCREEN_HEIGHT && my+y >= 0){
				VGA[(word)(screen_offset+mx+x)] = mouse->under[bitmap_offset];
			}
		}
		screen_offset+=SCREEN_WIDTH;
	}
}

