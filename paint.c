#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <sys/nearptr.h>
#include "video.h"
#include "glib.h"
#include "bitmap.h"
#include "mouse.h"

#define MAX_POINTS     20
#define MAX_VERSIONS   20


// global options
int done = 0, points = 0, drawing = 0, sur = 0;

// tool selected
int tool = 7, opt = 12;

// colours
byte color1 = 16, color2 = 15;

// principal bar button position
int pbaryi = 5, pbaryf = 20;
int pbarxi[9] = { 90, 115, 140, 165, 190, 215, 240, 265, 290};
int pbarxf[9] = {110, 135, 160, 185, 210, 235, 260, 285, 310};

// tool button position
int tbarxi[4] = {05, 30, 55, 04};
int tbarxf[4] = {25, 50, 75, 76};
int tbaryi[7] = {05, 25, 45, 65,  90, 110, 149};
int tbaryf[7] = {20, 40, 60, 80, 105, 125, 197};

// button effects
BITMAP press, release;

// Points to draw
int pointx[MAX_POINTS];
int pointy[MAX_POINTS];

// Clipboard and undo-redo
int clipx, clipy, clipe = 0;
int versions[MAX_VERSIONS];
int undo = 0, redo = 0;


// init variables
void init_vars(){
	load_bmp("PRESS.BMP", &press);
	load_bmp("RELEASE.BMP", &release);
	int i;
	for (i = 0; i < MAX_VERSIONS; i++){
		versions[i] = i;
	}
}

// new routine
void new_bitmap(){
	fill_Rectangle(80, 25, 320, 200, 15);
}

// exit routine
void exit_routine(){
	done = 1;
}

void save_clipboard(int x1, int y1, int x2, int y2){
	if (x1 > x2){
		clipe = x1;
		x1 = x2;
		x2 = clipe;
	}
	if (y1 > y2){
		clipe = y1;
		y1 = y2;
		y2 = clipe;
	}
	clipe = 1;
	clipx = x2 - x1;
	clipy = y2 - y1;
	FILE *fp;
	fp = fopen("CLPB", "wb");
	
	int i, index = (y1 << 8) + (y1 << 6) + x1;
	for (i = clipy; i; i--){
		fwrite(&VGA[index], sizeof(byte), clipx, fp);
		index += 320;
	}
	fclose(fp);
}

void paste_clipboard(int x, int y){
	if(clipe > 0){
		FILE *fp;
		if ((fp = fopen("CLPB", "rb")) == NULL){
			printf("Error opening clipboard");
			exit(1);
		}
		int i, j, index = (y << 8) + (y << 6) + x;
		for (i = clipy; i; i--){
			for (j = clipx; j; j--){
				if (x + (clipx - j) > 319){
					fgetc(fp);
					index ++;
				} else{
					VGA[index] = (byte)fgetc(fp);
					index ++;
				}
			}
			if (index >= 64000)
				break;
			else
				index += (320 - clipx);
		}
		fclose(fp);
	}
}

void save_undo(){
	int tmp, i;
	if (undo == MAX_VERSIONS){
		tmp = versions[0];
		for (i = 1; i < MAX_VERSIONS; i++){
			versions[i - 1] = versions[i];
		}
		versions[MAX_VERSIONS - 1] = tmp;
	} else{
		tmp = versions[undo];
		undo ++;
	}
	
	char file[8];
	sprintf(file, "ver_%d", tmp);
	FILE *fp;
	fp = fopen(file, "wb");
	
	int index = 8080;
	for (i = 175; i; i--){
		fwrite(&VGA[index], sizeof(byte), 240, fp);
		index += 320;
	}
	fclose(fp);
	redo = 0;
}

void undo_manage(){
	if(undo > 1){
		undo --;
		char file[8];
		sprintf(file, "ver_%d", versions[undo - 1]);
		FILE *fp;
		if ((fp = fopen(file,"rb")) == NULL){
			printf("Error opening file %s.\n",file);
			exit(1);
		}
		int i, j, index = 8080;
		for (i = 175; i; i--){
			for (j = 240; j; j--){
				VGA[index] = (byte)fgetc(fp);
				index ++;
			}
			index += 80;
		}
		fclose(fp);
		redo ++;
	}
}

void redo_manage(){
	if(redo > 0){
		char file[8];
		sprintf(file, "ver_%d", versions[undo]);
		FILE *fp;
		if ((fp = fopen(file,"rb")) == NULL){
			printf("Error opening file %s.\n",file);
			exit(1);
		}
		int i, j, index = 8080;
		for (i = 175; i; i--){
			for (j = 240; j; j--){
				VGA[index] = (byte)fgetc(fp);
				index ++;
			}
			index += 80;
		}
		fclose(fp);
		undo ++;
		redo --;
	}
}

// new interface
void load_interface(){
	BITMAP bmp;
	load_bmp("PBAR.BMP", &bmp);
	draw_bitmap(&bmp, VGA, 80, 0);
	
	load_bmp("TBAR.BMP", &bmp);
	draw_bitmap(&bmp, VGA, 0, 0);
	
	load_bmp("TLINE.BMP", &bmp);
	draw_bitmap(&bmp, VGA, 0, 85);
	
	load_bmp("PALETTE.BMP", &bmp);
	draw_bitmap(&bmp, VGA, 0, 130);
	new_bitmap();
	
	draw_transparent_bitmap(&press, VGA, tbarxi[1], tbaryi[2]);
	draw_transparent_bitmap(&press, VGA, tbarxi[0], tbaryi[4]);
}

void set_button(int oldx, int oldy, int newx, int newy, int fileid){
	draw_transparent_bitmap(&release, VGA, oldx, oldy);
	draw_transparent_bitmap(&press, VGA, newx, newy);
	if (fileid >= 0){
		opt = 12;
		BITMAP bmp;
		switch(fileid){
			case 0: // select
				load_bmp("EMPTY.BMP", &bmp);
				opt = -1;
				break;
			case 1: // line
				load_bmp("TLINE.BMP", &bmp);
				break;
			case 2: // rectangle
				load_bmp("TRECT.BMP", &bmp);
				break;
			case 3: // circle
				load_bmp("TCIRC.BMP", &bmp);
				break;
			case 4: // ellipse
				load_bmp("TCIRC.BMP", &bmp);
				break;
			case 5: // polygon
				load_bmp("TRECT.BMP", &bmp);
				break;
			case 6: // spray
				load_bmp("TSPRA.BMP", &bmp);
				break;
			case 7: // marker
				load_bmp("TLINE.BMP", &bmp);
				break;
			case 8: // eraser
				load_bmp("TLINE.BMP", &bmp);
				break;
			case 9: // fill
				load_bmp("TFILL.BMP", &bmp);
				break;
			case 10: // get colour
				load_bmp("EMPTY.BMP", &bmp);
				opt = -1;
				break;
			case 11: // text
				//load_bmp("TTEXT.BMP", &bmp);
				break;
			
		}
		draw_bitmap(&bmp, VGA, 0, 85);
		if (opt>0)
			draw_transparent_bitmap(&press, VGA, tbarxi[0], tbaryi[4]);
	}
}

manage_opt(int new_opt){
	if (opt != -1){
		int pass = 0;
		switch(tool){
			case 1: // line
				if (new_opt < 15) pass = 1;
				break;
			case 2: // rectangle
				if (new_opt < 17) pass = 1;
				break;
			case 3: // circle
				if (new_opt < 17) pass = 1;
				break;
			case 4: // ellipse
				if (new_opt < 17) pass = 1;
				break;
			case 5: // polygon
				if (new_opt < 17) pass = 1;
				break;
			case 6: // spray
				if (new_opt < 15) pass = 1;
				break;
			case 7: // marker
				if (new_opt < 15) pass = 1;
				break;
			case 8: // eraser
				if (new_opt < 15) pass = 1;
				break;
			case 9: // fill
				if (new_opt < 15) pass = 1;
				break;
			case 11: // text
				//load_bmp("TTEXT.BMP", &bmp);
				break;
		}
		if (pass > 0){
			draw_transparent_bitmap(&release, VGA, tbarxi[opt%3], tbaryi[opt/3]);
			draw_transparent_bitmap(&press, VGA, tbarxi[new_opt%3], tbaryi[new_opt/3]);
			opt = new_opt;
		}
	}
}

void manage_pbar(int pbarid){
	switch(pbarid){
		case 0: // new
			new_bitmap();
			break;
		case 1: // open
			break;
		case 2: // save
			break;
		case 3: // undo
			undo_manage();
			break;
		case 4: // redo
			redo_manage();
			break;
		case 5: // cut
			if (tool == 0){
				undo --;
				redo ++;
				redo_manage();
				save_clipboard(pointx[0], pointy[0], pointx[1], pointy[1]);
				fill_Rectangle(pointx[0], pointy[0], pointx[1], pointy[1], 15);
				save_undo();
			}
			break;
		case 6: // copy
			if (tool == 0){
				undo --;
				redo ++;
				redo_manage();
				save_clipboard(pointx[0], pointy[0], pointx[1], pointy[1]);
			}
			break;
		case 7: // paste
			if (tool == 0){
				undo --;
				redo ++;
				redo_manage();
				paste_clipboard(pointx[0], pointy[0]);
				points = 0;
				save_undo();
			}
			break;
		case 8: // exit
			exit_routine();
			break;
	}
}

void manage_click(int x, int y, int new_x, int new_y){
	if (x > 80){
		if (y > 25){
			drawing = 1;
			switch(tool){
				case 0: // select
					if (points == 0){
						undo --;
						redo ++;
						redo_manage();
						pointx[0] = x;
						pointy[0] = y;
						points ++;
					} else{
						undo --;
						redo ++;
						redo_manage();
						pointx[1] = x;
						pointy[1] = y;
						draw_Rectangle(pointx[0], pointy[0], x, y, 30, 1);
						points = 0;
						drawing = 0;
					}
					break;
				case 1: // line
					if (points == 0){
						pointx[0] = x;
						pointy[0] = y;
						points ++;
					} else{
						undo --;
						redo ++;
						redo_manage();
						draw_Line(pointx[0], pointy[0], x, y, color1, (opt % 3) + 1);
						points = 0;
						drawing = 0;
						save_undo();
					}
					break;
				case 2: // rectangle
					if (points == 0){
						pointx[0] = x;
						pointy[0] = y;
						points ++;
					} else{
						undo --;
						redo ++;
						redo_manage();
						switch(opt){
							case 15:
								fill_Rectangle(pointx[0], pointy[0], x, y, color1);
								break;
							case 16:
								fill_Rectangle(pointx[0], pointy[0], x, y, color2);
								draw_Rectangle(pointx[0], pointy[0], x, y, color1, 1);
								break;
							default:
								draw_Rectangle(pointx[0], pointy[0], x, y, color1, (opt % 3) + 1);
								points = 0;
								drawing = 0;
								break;
						}
						points = 0;
						drawing = 0;
						save_undo();
					}
					break;
				case 3: // circle
					if (points == 0){
						pointx[0] = x;
						pointy[0] = y;
						points ++;
					} else{
						undo --;
						redo ++;
						redo_manage();
						int r = abs(x - pointx[0]);
						switch(opt){
							case 15:
								fill_Circle(pointx[0], pointy[0], r, color1);
								break;
							case 16:
								fill_Circle(pointx[0], pointy[0], r, color2);
								draw_Circle(pointx[0], pointy[0], r, color1, 1);
								break;
							default:
								draw_Circle(pointx[0], pointy[0], r, color1, (opt % 3) + 1);
								points = 0;
								drawing = 0;
								break;
						}
						points = 0;
						drawing = 0;
						save_undo();
					}
					break;
				case 4: // ellipse
					if (points == 0){
						pointx[0] = x;
						pointy[0] = y;
						points ++;
					} else{
						undo --;
						redo ++;
						redo_manage();
						int rx = abs(x - pointx[0]);
						int ry = abs(y - pointy[0]);
						switch(opt){
							case 15:
								fill_Ellipse(pointx[0], pointy[0], rx, ry, color1);
								break;
							case 16:
								fill_Ellipse(pointx[0], pointy[0], rx, ry, color2);
								draw_Ellipse(pointx[0], pointy[0], rx, ry, color1, 1);
								break;
							default:
								draw_Ellipse(pointx[0], pointy[0], rx, ry, color1, (opt % 3) + 1);
								break;
						}
						points = 0;
						drawing = 0;
						save_undo();
					}
					break;
				case 5: // polygon
					if (points < MAX_POINTS - 1){
						pointx[points] = x;
						pointy[points] = y;
						points ++;
					} else{
						undo --;
						redo ++;
						redo_manage();
						pointx[points] = x;
						pointy[points] = y;
						points ++;
						switch(opt){
							case 15:
								fill_Polygon(points, pointx, pointy, color1);
								break;
							case 16:
								fill_Polygon(points, pointx, pointy, color2);
								draw_Polygon(points, pointx, pointy, color1, 1);
								break;
							default:
								draw_Polygon(points, pointx, pointy, color1, (opt % 3) + 1);
								break;
						}
						points = 0;
						drawing = 0;
						save_undo();
					}
					break;
				case 6: // spray
					switch(opt){
						case 12:
							put_pixel(x - 5 + rand()%10, y - 5 + rand()%10, color1);
							break;
						case 13:
							put_pixel(x - 10 + rand()%20, y - 10 + rand()%20, color1);
							break;
						case 14:
							put_pixel(x - 15 + rand()%30, y - 15 + rand()%30, color1);
							break;
					}
					if (sur > 0){
						sur = 0;
						save_undo();
					}
					break;
				case 7: // Marker
					drawing = 1;
					draw_Line(x, y, new_x, new_y, color1, (opt % 3) + 1);
					if (sur > 0){
						sur = 0;
						save_undo();
					}
					break;
				case 8: // eraser
					drawing = 1;
					draw_Line(x, y, new_x, new_y, 15, (opt % 3) + 1);
					if (sur > 0){
						sur = 0;
						save_undo();
					}
					break;
				case 9: // fill
					switch(opt){
						case 12:
							solid_Fill(x, y, color1, get_color(x, y));
							break;
						case 13:
							pattern1_Fill(x, y, color1, color2, get_color(x, y), (320*y+x)%4);
							break;
						case 14:
							pattern2_Fill(x, y, color1, color2, get_color(x, y), x%4, y%4);
							break;
						case 15:
							break;
						case 16:
							break;
						case 17:
							break;
					}
					save_undo();
					break;
				case 10: // get colour
					color1 = get_color(x, y);
					super_patch(0, color1);
					break;
				case 11: // text
					break;
				default:
					break;
			}
		} else { // Principal bar
			if(!drawing){
				if (y > pbaryi && y < pbaryf){
					int i;
					for (i = 0; i < 9; i++){
						if(pbarxi[i] < x && pbarxf[i] > x){
							manage_pbar(i);
							i = 10;
						}
					}
				}
			}
		}
	} else { // Tools bar
		if(!drawing){
			if (y > tbaryi[6]){
				if (y < tbaryf[6] && x > tbarxi[3] && x < tbarxf[3]){
					color1 = get_color(x, y);
					super_patch(0, color1);
				}
			}else{
				int i, j;
				for(j = 0; j < 4; j++){
					if(tbaryi[j] < y && tbaryf[j] > y){
						for(i = 0; i < 3; i++){
							if(tbarxi[i] < x && tbarxf[i] > x){
								int tmp = j*3 + i;
								set_button(tbarxi[tool%3], tbaryi[tool/3], tbarxi[i], tbaryi[j], tmp);
								tool = tmp;
								i = 9;
							}
						}
						j = 9;
					}
				}
				for(j = 4; j < 6; j++){
					if(tbaryi[j] < y && tbaryf[j] > y){
						for(i = 0; i < 3; i++){
							if(tbarxi[i] < x && tbarxf[i] > x){
								manage_opt(j*3 + i);
								i = 9;
							}
						}
						j = 9;
					}
				}
			}
		}
	}
}

void manage_click_r(int x, int y){
	if (y > tbaryi[6]){
		if (y < tbaryf[6] && x > tbarxi[3] && x < tbarxf[3]){
			color2 = get_color(x, y);
			super_patch(1, color2);
		}
	}
	if (x > 80){
		if (y > 25 && tool == 10){
			color2 = get_color(x, y);
			super_patch(1, color2);
		}
		if (y > 25 && tool == 5){
			//undo_manage();
			undo --;
			redo ++;
			redo_manage();
			pointx[points] = x;
			pointy[points] = y;
			points ++;
			switch(opt){
				case 15:
					fill_Polygon(points, pointx, pointy, color1);
					break;
				case 16:
					fill_Polygon(points, pointx, pointy, color2);
					draw_Polygon(points, pointx, pointy, color1, 1);
					break;
				default:
					draw_Polygon(points, pointx, pointy, color1, (opt % 3) + 1);
					break;
			}
			points = 0;
			drawing = 0;
			save_undo();
		}
	}
}

void pseudo_draw(int x, int y){
	undo --;
	redo ++;
	redo_manage();
	int i;
	switch (tool){
		case 0: // select
			draw_Rectangle(pointx[0], pointy[0], x, y, 29, 1);
			break;
		case 1: // line
			draw_Line(pointx[0], pointy[0], x, y, 29, 1);
			break;
		case 2: // rectangle
			draw_Rectangle(pointx[0], pointy[0], x, y, 29, 1);
			break;
		case 3: // circle
			draw_Circle(pointx[0], pointy[0], abs(x - pointx[0]), 29, 1);
			break;
		case 4: // Ellipse
			draw_Ellipse(pointx[0], pointy[0], abs(x - pointx[0]), abs(y - pointy[0]), 29, 1);
			break;
		case 5: // Polygon
			for (i = 0; i < points - 1; i++){
				draw_Line(pointx[i], pointy[i], pointx[i + 1], pointy[i + 1], 29, 1);
			}
			draw_Line(pointx[0], pointy[0], x, y, 29, 1);
			draw_Line(pointx[points - 1], pointy[points - 1], x, y, 29, 1);
			break;
	}
}

int main(){
	BITMAP bmp;
	MOUSE mouse;
	MOUSEBITMAP *mouse_bitmap;
	
	word redraw, press, release, press2, release2;
	sword dx = 0, dy = 0, new_x, new_y;
	int x, y;
	
	set_VGA_256();
	
	init_vars();
	load_interface();
	save_undo();
	
	if ((mouse_bitmap = (MOUSEBITMAP *)malloc(sizeof(MOUSEBITMAP))) == NULL){
		printf("Error allocating memory for pointer.\n");
		exit(1);
	}
	
	load_bmp("POINTER.BMP", &bmp);
	mouse.bmp = mouse_bitmap;
	if (!init_mouse(&mouse)){
		printf("Mouse not found.\n");
		exit(1);
	}
	
	for(y = 0; y < MOUSE_HEIGHT; y++){
		for(x = 0; x < MOUSE_WIDTH; x++){
			mouse_bitmap->data[x+y*MOUSE_WIDTH] = bmp.data[x+y*bmp.width];
		}
	}
	mouse_bitmap->hot_x = 0;
	mouse_bitmap->hot_y = 0;
	mouse_bitmap->next = NULL;
	
	free(bmp.data);
	
	new_x = mouse.x;
	new_y = mouse.y;
	redraw = 0xFFFF;
	
	show_mouse(&mouse, VGA);
	
	while (!done){
	
		if (redraw){
			wait_for_retrace();
			hide_mouse(&mouse, VGA);
			
			if(mouse.button1){
				manage_click(mouse.x, mouse.y, new_x, new_y);
			} else if(mouse.button2){
				manage_click_r(mouse.x, mouse.y);
			} else{
				if (points)
					pseudo_draw(mouse.x, mouse.y);
			}
			mouse.x = new_x;
			mouse.y = new_y;
			show_mouse(&mouse, VGA);
			redraw = 0;
		}

		do {
			get_mouse_motion(&dx, &dy);
			press    = get_mouse_press(LEFT_BUTTON);
			press2   = get_mouse_press(RIGHT_BUTTON);
			release  = get_mouse_release(LEFT_BUTTON);
			release2 = get_mouse_release(RIGHT_BUTTON);
		} while (dx==0 && dy==0 && press==0 && release==0 && press2==0 && release2==0 );

		if (press){
			mouse.button1 = 1;
			redraw = 1;
		}
		if (release){
			mouse.button1 = 0;
			drawing = 0;
			if (tool > 5 && tool < 9){
				redraw = 1;
				sur = 1;
			}
		}
		if (press2){
			mouse.button2 = 1;
			redraw = 1;
		}
		if (release2){
			mouse.button2 = 0;
		}
		if (dx || dy) {
			new_x = mouse.x+dx;
			new_y = mouse.y+dy;
			if (new_x < 0)   new_x = 0;
			if (new_y < 0)   new_y = 0;
			if (new_x > 319) new_x = 319;
			if (new_y > 199) new_y = 199;
			redraw = 1;
		}
	}
	free(mouse_bitmap);
	set_TEXT();

	return 0;
}
