#define SCREEN_WIDTH        320
#define SCREEN_HEIGHT       200
#define NUM_COLORS          256

typedef unsigned char  byte;
typedef unsigned short word;
typedef unsigned long  dword;

byte* VGA;

extern void set_VGA_256();
extern void set_TEXT();
extern void put_pixel(int x, int y, byte color);
extern byte get_color(int x, int y);
extern void super_patch(int id, byte color);

