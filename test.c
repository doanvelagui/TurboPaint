#include <sys/nearptr.h>
#include <stdio.h>
#include "video.h"
#include "bitmap.h"
#include "glib.h"

int main(){
    set_VGA_256();
    
	int i;
	
    /* Testing draw line
	for (i = 2000; i; i--){
		draw_Line(rand()%280+20,rand()%160+20,rand()%280+20,rand()%160+20,rand()%256);
	}
	//*/
	
	/* Testing draw line with thick
	for (i = 20; i; i--){
		thick_Line(rand()%280+20,rand()%160+20,rand()%280+20,rand()%160+20,rand()%256, 3);
	}
	//*/
	
    /* Testing draw circle
    for (i = 25; i; i--){
		draw_Circle(100,100,(i << 2),rand()%256);
	}
	//*/
	
	/* Testing draw rectangle
	draw_Rectangle(50, 50, 150, 150, 0xAA);
	draw_Rectangle(151, 51, 51, 151, 0xBB);
	draw_Rectangle(152, 152, 52, 52, 0x99);
	draw_Rectangle(53, 153, 153, 53, 0x88);
    //*/
	
	/* Testing fill and thick rectangle
	fill_Rectangle(30, 30, 200, 189, 0xAA);
	thick_Rectangle(30, 30, 200, 189, 0x77, 3);
	thick_Rectangle(50, 50, 175, 175, 0x77, 2);
	thick_Rectangle(70, 70, 155, 155, 0x77, 1);
	
	//*/
	
	/* Testing fill circle
	fill_Circle(100, 100, 45, 0xBB);
	fill_Circle(100, 100, 30, 0x88);
	fill_Circle(100, 100, 15, 0x99);
	//*/
	
	/* Testing draw polygon
	int x[5];
	int y[5];
	for (i = 0; i < 5; i++){
		x[i] = rand()%320;
		y[i] = rand()%200;
	}
	draw_Polygon(5, x, y, 0xAA);
	//*/
	
	/* Testing fill polygon
	int x[5];
	int y[5];
	for (i = 0; i < 5; i++){
		x[i] = rand()%320;
		y[i] = rand()%200;
	}
	//fill_Polygon(5, x, y, 0xAA);
	//*/
	
	/* Testing draw ellipse
	draw_Ellipse(100, 100, 40, 20, 0x77);
	draw_Ellipse(100, 100, 20, 40, 0x77);
	//*/
	
	/* Testing fill ellipse
	fill_Ellipse(100, 100, 40, 20, 0x77);
	fill_Ellipse(100, 100, 20, 40, 0x77);
	//*/
	
	/* Testing draw [transparent] bitmap
	int x, y;
	BITMAP bmp;
	load_bmp("PRB.BMP",&bmp);
	draw_transparent_bitmap(&bmp, 0, 0);
	draw_bitmap(&bmp, 30, 30);
	draw_transparent_bitmap(&bmp, 60, 60);
	//*/
	
	/* Testing save_bitmap_from_vga
	fill_Rectangle(60,60,65,65,0x8);
	fill_Rectangle(65,65,70,70,0x9);
	fill_Rectangle(60,65,65,70,0x7);
	fill_Rectangle(65,60,70,65,0x6);
	save_bitmap_from_vga("prb.bmp", 60, 60, 70, 70);
	//*/
    
	/* Testing flood fill
	//flood_Fill(100, 100, 0xAA, 0);
	draw_Ellipse(100, 100, 40, 50, 15);
	flood_Fill(100, 100, 0x77, 0);
	draw_Ellipse(100, 100, 40, 10, 15);
	flood_Fill(100, 100, 0xAA, 0x77);
	//*/
	
	// set_TEXT();
	
	return;
}
