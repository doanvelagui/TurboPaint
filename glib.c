#include "video.h"
#include "glib.h"

// Manage thickness
void thickness(int x, int y, byte color, int thick){
	if (thick > 0)
		put_pixel(x, y, color);
	if (thick > 1){
		put_pixel(x + 1, y, color);
		put_pixel(x, y + 1, color);
		put_pixel(x + 1, y + 1, color);
	}
	if (thick > 2){
		put_pixel(x - 1, y, color);
		put_pixel(x, y - 1, color);
		put_pixel(x + 1, y - 1, color);
		put_pixel(x - 1, y + 1, color);
		put_pixel(x - 1, y - 1, color);
	} // Could add more thickness
}

// Draw an horizontal line
void draw_H_Line(int x1, int x2, int y, byte color, int thick){
	int i, x = x1, sx = sign(x2 - x1);
	for (i = abs(x2 - x1); i; i--){
		thickness(x, y, color, thick);
		x += sx;
	}
}

void draw_H_Linef(int x1, int x2, int y, byte color, int thick){
	int i, x = x1, sx = sign(x2 - x1);
	for (i = abs(x2 - x1); i; i--){
		put_pixelt(x, y, color);
		x += sx;
	}
}

// Draw an vertical line
void draw_V_Line(int x, int y1, int y2, byte color, int thick){
	int i, y = y1, sy =  sign(y2 - y1);
	for (i = abs(y2 - y1); i; i--){
		thickness(x, y, color, thick);
		y += sy;
	}
}

void draw_V_Linef(int x, int y1, int y2, byte color, int thick){
	int i, y = y1, sy =  sign(y2 - y1);
	for (i = abs(y2 - y1); i; i--){
		put_pixelt(x, y, color);
		y += sy;
	}
}

// Draw line with Bresenham algorithm
void draw_Line(int x1, int y1, int x2, int y2, byte color, int thick){
    int i, dx, dy, dxabs, dyabs, ddx, ddy, sx, sy, px, py, x, y;
    
    // distance from points in axis x and y
    dx = x2 - x1;
	if (dx == 0){ draw_V_Line(x1, y1, y2, color, thick); return; }
	dy = y2 - y1;
	if (dy == 0){ draw_H_Line(x1, x2, y2, color, thick); return; }
    dxabs = abs(dx);
    dyabs = abs(dy);
    
    // used for P_k parameter in Bresenham algorithm
    ddx = (dxabs << 1);
    ddy = (dyabs << 1);
    px = ddy - dxabs;
    py = ddx - dyabs;
    
    // parameters to plot points
    x = x1;
    y = y1;
    sx = sign(dx);
    sy = sign(dy);
    
    // plotting
    if (dxabs >= dyabs){ // case abs(m) <= 1
        for (i=dxabs; i; i--) {
			thickness(x, y, color, thick);
            x += sx;
            if (px >= 0){
                y += sy;
                px -= ddx;
            }
            px += ddy;
        }
    } else{ // case abs(m) >= 1
        for (i=dyabs; i; i--) {
            thickness(x, y, color, thick);
            y += sy;
            if (py >= 0){
                x += sx;
                py -= ddy;
            }
            py += ddx;
        }
    }
}


// Draw a rectangle
void draw_Rectangle(int x1, int y1, int x2, int y2, byte color, int thick){
	draw_Line(x1, y1, x2, y1, color, thick);
	draw_Line(x2, y1, x2, y2, color, thick);
	draw_Line(x2, y2, x1, y2, color, thick);
	draw_Line(x1, y2, x1, y1, color, thick);
	thickness(x1, y1, color, thick);
	thickness(x1, y2, color, thick);
	thickness(x2, y1, color, thick);
	thickness(x2, y2, color, thick);
	return;
}

// Draw a filled rectangle
void fill_Rectangle(int x1, int y1, int x2, int y2, byte color){
	int i, j, dx, dy, sx, sy, x, y = y1;
	
	dx = abs(x2 - x1);				// weight and height of rectangle
	dy = abs(y2 - y1);
	
	sx = sign(x2 - x1);				// step for x and y
	sy = sign(y2 - y1);
	
	for (i = dy; i; i--){
		x = x1;
		for (j = dx; j; j--){
			put_pixel(x, y, color);
			x += sx;
		}
		y += sy;
	}
}

void fill_Rectanglef(int x1, int y1, int x2, int y2, byte color){
	int i, j, dx, dy, sx, sy, x, y = y1;
	
	dx = abs(x2 - x1);				// weight and height of rectangle
	dy = abs(y2 - y1);
	
	sx = sign(x2 - x1);				// step for x and y
	sy = sign(y2 - y1);
	
	for (i = dy; i; i--){
		x = x1;
		for (j = dx; j; j--){
			put_pixelt(x, y, color);
			x += sx;
		}
		y += sy;
	}
}

// Draw a circle
void draw_Circle(int xc, int yc, int rad, byte color, int thick){
    int x = 0, y = rad, dx = 0, dy = (rad << 1);
    double param = 5/4 - rad;
	
    while (x <= y) {
        thickness(xc + x, yc + y, color, thick);
        thickness(xc - x, yc + y, color, thick);
        thickness(xc + x, yc - y, color, thick);
        thickness(xc - x, yc - y, color, thick);
        thickness(xc + y, yc + x, color, thick);
        thickness(xc + y, yc - x, color, thick);
        thickness(xc - y, yc + x, color, thick);
        thickness(xc - y, yc - x, color, thick);
		x ++;
        dx += 2;
        if (param <= 0) {
            param += dx + 1;
        } else {
            y --;
            dy -= 2;
            param += dx + 1 - dy;
        }
    }
    return;
}

void draw_Circlef(int xc, int yc, int rad, byte color, int thick){
    int x = 0, y = rad, dx = 0, dy = (rad << 1);
    double param = 5/4 - rad;
	
    while (x <= y) {
        put_pixelt(xc + x, yc + y, color);
        put_pixelt(xc - x, yc + y, color);
        put_pixelt(xc + x, yc - y, color);
        put_pixelt(xc - x, yc - y, color);
        put_pixelt(xc + y, yc + x, color);
        put_pixelt(xc + y, yc - x, color);
        put_pixelt(xc - y, yc + x, color);
        put_pixelt(xc - y, yc - x, color);
		x ++;
        dx += 2;
        if (param <= 0) {
            param += dx + 1;
        } else {
            y --;
            dy -= 2;
            param += dx + 1 - dy;
        }
    }
    return;
}

// Draw a filled Circle
void fill_Circle(int xc, int yc, int rad, byte color){
	int x = 0, y = rad, dx = 0, dy = (rad << 1);
    double param = 5/4 - rad;
	
    while (x <= y) {
		draw_V_Line(xc + x, yc - y, yc + y, color, 1);
		draw_V_Line(xc - x, yc - y, yc + y, color, 1);
		x ++;
        dx += 2;
        if (param <= 0) {
            param += dx + 1;
        } else {
			y --;
            dy -= 2;
            param += dx + 1 - dy;
			draw_V_Line(xc + y, yc - x, yc + x, color, 1);
			draw_V_Line(xc - y, yc - x, yc + x, color, 1);
        }
    }
    return;
}

// Draw a ellipse
void draw_Ellipse(int xc, int yc, int rx, int ry, byte color, int thick){
	int x = 0, y = ry;
	int rx2 = rx*rx;
	int ry2 = ry*ry;
	int two_rx2 = (rx2 << 1);
	int two_ry2 = (ry2 << 1);
	
	int px = 0;							// parameter for x
	int py = ((y*rx2) << 1);			// parameter for y
	int p  = (ry2) - (rx2 * ry) + (rx2 >> 2);
	
	while(px < py){
		thickness(xc+x, yc+y, color, thick);
		thickness(xc+x, yc-y, color, thick);
		thickness(xc-x, yc+y, color, thick);
		thickness(xc-x, yc-y, color, thick);
		
		x ++;
		px += two_ry2;
		if(p>=0){
			y --;
			py -= two_rx2;
			p -= py;
		}
		p += px + ry2;
	}
	
	p = (int)(((float)x+0.5)*((float)x+0.5)*ry2 + (y-1)*(y-1)*rx2 - rx2*ry2 + 0.5);
    
	while(y >= 0){
		thickness(xc+x, yc+y, color, thick);
		thickness(xc+x, yc-y, color, thick);
		thickness(xc-x, yc+y, color, thick);
		thickness(xc-x, yc-y, color, thick);
		
		y --;
		py -= two_rx2;
		if(p <= 0){
			x ++;
			px += two_rx2;
			p += px;
		}
		p += rx2 - py;
	}
}

// Draw a filled ellipse
void fill_Ellipse(int xc, int yc, int rx, int ry, byte color){
	int x = 0, y = ry;
	int rx2 = rx*rx;
	int ry2 = ry*ry;
	int two_rx2 = (rx2 << 1);
	int two_ry2 = (ry2 << 1);
	
	int px = 0;							// parameter for x
	int py = ((y*rx2) << 1);			// parameter for y
	int p  = (ry2) - (rx2 * ry) + (rx2 >> 2);
	
	while(px < py){
		draw_V_Line(xc+x, yc-y, yc+y, color, 1);
		draw_V_Line(xc-x, yc-y, yc+y, color, 1);
		
		x ++;
		px += two_ry2;
		if(p>=0){
			y --;
			py -= two_rx2;
			p -= py;
		}
		p += px + ry2;
	}
	
	p = (int)(((float)x+0.5)*((float)x+0.5)*ry2 + (y-1)*(y-1)*rx2 - rx2*ry2 + 0.5);
    draw_V_Line(xc+x, yc-y, yc+y, color, 1);
	draw_V_Line(xc-x, yc-y, yc+y, color, 1);
	while(y >= 0){
		y --;
		py -= two_rx2;
		if(p <= 0){
			x ++;
			px += two_rx2;
			p += px;
			draw_V_Line(xc+x, yc-y, yc+y, color, 1);
			draw_V_Line(xc-x, yc-y, yc+y, color, 1);
		}
		p += rx2 - py;
	}
}

// Draw a polygon
void draw_Polygon(int size, int* x, int* y, byte color, int thick){
	int i;
	for (i = 1; i < size; i++){
		draw_Line(x[i-1], y[i-1], x[i], y[i], color, thick);
	}
	draw_Line(x[size-1], y[size-1], x[0], y[0], color, thick);
	return;
}

int pointInPolygon(float x, float y, int size, float* polyX, float* polyY){
	int   i, j = size-1 ;
	int   oddNodes = 0;

	for (i=0; i<size; i++) {
		if (polyY[i]<y && polyY[j]>=y ||  polyY[j]<y && polyY[i]>=y){
			if (polyX[i]+(y-polyY[i])/(polyY[j]-polyY[i])*(polyX[j]-polyX[i])<x){
				oddNodes = !oddNodes;
			}
		}
		j = i;
	}
	return oddNodes; 
}

void fill_Polygon(int size, int* x, int* y, byte color){
	int i, j, max_x = x[0], max_y = y[0], min_x = x[0], min_y = y[0];
	for (i = 1; i < size; i++){
		if(x[i] > max_x){
			max_x = x[i];
		}
		if(y[i] > max_y){
			max_y = y[i];
		}
		if(x[i] < min_x){
			min_x = x[i];
		}
		if(y[i] < min_y){
			min_y = y[i];
		}
	}
	float xdot[size], ydot[size];
	for (i = 0; i < size; i++){ 
		xdot[i] = (float)x[i];
		ydot[i] = (float)y[i];
	}
	int pintar = 0;
	for (j = min_y; j <= max_y; j++){
		for (i = min_x; i <= max_x; i++){
			if (pointInPolygon((float)i, (float)j, size, xdot, ydot))
				put_pixel(i, j, color);
		}
	}
	return;
}

void fill_Polygonf(int size, float * x, float * y, byte color){
	int i, j;
	float max_x = x[0], max_y = y[0], min_x = x[0], min_y = y[0];
	for (i = 1; i < size; i++){
		if(x[i] > max_x){
			max_x = x[i];
		}
		if(y[i] > max_y){
			max_y = y[i];
		}
		if(x[i] < min_x){
			min_x = x[i];
		}
		if(y[i] < min_y){
			min_y = y[i];
		}
	}
	for (j = min_y; j <= max_y; j++){
		for (i = min_x; i <= max_x; i++){
			if (pointInPolygon((float)i, (float)j, size, x, y))
				put_pixelt(i, j, color);
		}
	}
	return;
}

void solid_Fill(int x, int y, byte color, byte oldColor){
	if(get_color(x, y) == oldColor){
		put_pixel(x, y, color);
		solid_Fill(x + 1, y, color, oldColor);
		solid_Fill(x - 1, y, color, oldColor);
		solid_Fill(x, y + 1, color, oldColor);
		solid_Fill(x, y - 1, color, oldColor);
	}
}

void pattern1_Fill(int x, int y, byte color1, byte color2, byte oldColor, int i){
	if(get_color(x, y) == oldColor){
		if (i < 2)
			put_pixel(x, y, color1);
		else
			put_pixel(x, y, color2);
		pattern1_Fill(x + 1, y, color1, color2, oldColor, (i + 1) % 4);
		pattern1_Fill(x - 1, y, color1, color2, oldColor, (i + 3) % 4);
		pattern1_Fill(x, y + 1, color1, color2, oldColor, (i + 1) % 4);
		pattern1_Fill(x, y - 1, color1, color2, oldColor, (i + 3) % 4);
	}
}

void pattern2_Fill(int x, int y, byte color1, byte color2, byte oldColor, int i, int j){
	if(get_color(x, y) == oldColor){
		if (i < 2 && j < 2)
			put_pixel(x, y, color1);
		else
			put_pixel(x, y, color2);
		pattern2_Fill(x + 1, y, color1, color2, oldColor, (i + 1) % 4, j);
		pattern2_Fill(x - 1, y, color1, color2, oldColor, (i + 3) % 4, j);
		pattern2_Fill(x, y + 1, color1, color2, oldColor, i, (j + 1) % 4);
		pattern2_Fill(x, y - 1, color1, color2, oldColor, i, (j + 3) % 4);
	}
}


// Mathematical sign function
int sign(int val){
	return ( (val<0) ? -1 : (val>0) ? 1 : 0 );
}

