## Content Table:

1. DESCRIPTION / RUNNING
1. INSTALL AND COMPILE
1. FEATURES
1. PENDING (TODO LIST)
1. EXTRA
1. CONTACT (again)

# 1. DESCRIPTION

TurboPaint (TURBOPAINT.EXE) is a project for my Computer Graphics class. This
project runs over DOSBox and is compiled with DJGPP compiler (a GCC port for
Windows). The interface of TurboPaint is loaded with some bitmaps, this have
to be created before of execution of TurboPaint, or an error will show if you
execute it on DOSBox. This project is focused in manage some algorithms for
draw lines (Bresenham Algorithm), circles (MidPoint Algorithm), ellipses (Mid-
Point Algorithm), rectangles, polygons (ScanLine Algorithm) and filling.

Turbo Paint GENerator of INTeface (TPGENINT.EXE) is a program with the object
of create the bitmap files used by TurboPaint. You shall to run this program
every time TurboPaint shows an error where doesn't find a file to loading the
interface.

TRansformation 2D (TR2D.EXE) correspond to the second project for my Computer
Graphics class. Like previous, this use DOSBos as execution environment with
DJGPP compiler. Some images are necessary to load the interface of this project
succesfully (see next program). This project are focused in manage different
transformations of polygons with matrixes and vectors products. This transfor-
mations are: translation, rotation, escalation, shear, and flip or mirroring.

INTerface GENerator (INTGEN.EXE) has the goal of create the bitmap required
by TR2D.EXE program to load the interface. Like TPGENINT.EXE, this program is
necesary to run when an error occurs with TR2D.EXE at loading interface.

# 2. INSTALL AND COMPILE
To Compile and Run is is necesary to install:
   1. DJGPP
   2. DOSBox

   To download DJGPP, visit www.delorie.com/djgpp/zip-picker.html. The options
   to use are:
      OPTION                           SELECTION
      FTP Site                         Pick one for me

      Basic Functionality              Build anr run programs with DJGPP
                                       MS-DOS, OpenDOS, PD-DOS
                                       No
                                       C, Assembler
      Integrated Development Enviro... [none]
                                       No
      Extra Stuff                      [none]

   Then click "Tell me which files I need" button, and download all the files
   listed at zip-picker.cgi. These zip files are: djdev##.zip, faq##.zip,
   bnu##.zip, gcc##.zip, mak##.zip, csdpmi#.zip (where # are a numeric value
   and ## are a numeric value and a letter, that represents the version).

   To install DJGPP just unzip these files in an correct way:
      If you are working with Mac OS X, just open the terminal, change to the
      directory where are all the zip files and hit: unzip /*.zip. It shall
      unzip the files and create the correct directories.
      Using other OS, you should be careful of don't create files like djdev##/
      because it won't work, use other unzip program to get the next directories
         copying (file, from now 'f')
         copying.dj (f)
         copying.lib (f)
         djgpp.env (f)
         readme.1st (f)
         bin (directory, from now 'd')
         FAQ (d)
         gnu (d)
         include (d)
         info (d)
         lib (d)
         libexec (d)
         man (d)
         manifest (d)
         share (d)
         tmp (d)

   To download DOSBox go to www.dosbox.com/download.php?main=1, and download
   it to your OS. The install is very easy, in Mac OS X just open the DMG and
   drag the app to your Applications Folder. After install, run DOSBox almost
   once (principally in Mac OS X, to create configurations files).

   Some configurations are needed to compile with DJGPP at DOSBox, first open
   the DOSBox configuration file:
      In Windows, execute the program DOSBox 0.74 Configuration, and it will
      open the configurations file.
      In Mac OS X, go to ~/Library/Preferences/ directory and open the DosBox
      0.74 Configurations file.

   After opened the configuration file, go to the end of this and add the next
   lines in the configuration file:
      mount C: /Some/directory/Containing/DJGPP/Folder
      set PATH=C:\Path\To\DJGPP\bin;%PATH%
      set DJGPP=C:\Path\To\DJGPP\djgpp.env

   Now when you start DOSBox again, these lines are executed automatically, and
   you can use gcc command to compile your c scripts.


For compile the first project use next command at DOSBox:
   gcc paint.c video.c glib.c bitmap.c mouse.c -o TPAINT.EXE
For the second project use:
   gcc trans.c video.c glib.c bitmap.c mouse.c -o TR2D.EXE

# 3. FEATURES
   - Methods to save, open and draw bitmaps
   - Bresenham algorithm for draw lines
   - Midpoint algorithm for draw circles and ellipses
   - ScanLine algorithm for draw filled polygons
   - Fill algorithm with patterns.
   - Escalation of objects
   - Translation of objects
   - Rotation of objects
   - Flip of objects
   - Shear of objects

# 4. PENDING (TODO LIST)
   Both projects:
   - Add help menu in both projects.
   - Add more documentation in code.

   TurboPaint:
   - Optimize preview of drawing objects.
   - Solve some problems with undo/redo at tools: pencil/marker, eraser, spray.
   - Solve problems for fill algotithm, the program die when the fill area is
     big, options: use heap instead of stack.
   - Create routines to save, open, new methods, actually save and open don't
     work, and new just erase screen, and don't manage undo/redo.
   - Add creation of patterns.
   - Add text (some ideas is to create a bitmap with alphabet and manage this
     as sprites).
   - Add text, manage keyboard interrutpions with assembler code.
   - Manage Palette Colors.

   Transformations 2D:
   - Manage keep pressed a button, actually this only makes an transformation
     every time the button is clicked.
   - Optimize refresh of objects. An great idea apported is to draw the polygon
     without fill and then, when button is released, fill the polygon.
   - Add reset button for transformations.
   - Draw Pivot will be optimized by manage of objects as sprites (to see if
     exist an collision between two objects.
   - Add keyboard commands, some times click is bored.
   - Add keyboard commands, manage keyboard interruptions with assembler code.

# 5. EXTRA
Some sites helpfull:
   1. http://geco.mines.edu/guide/pgcc.html
   2. http://gcc.gnu.org/
   3. http://www.codeproject.com/Articles/15971/Using-Inline-Assembly-in-C-C
   4. http://www.supernovah.com/Tutorials/Assembly2.php
   5. http://www.csi.ucd.ie/staff/jcarthy/home/alp/8086Registers.pdf
   6. http://www.ctyme.com/rbrown.htm
   7. http://bos.asmhackers.net/docs/vga_without_bios/docs/palettesetting.pdf
   8. http://www.brackeen.com/vga/index.html

# 6. CONTACT AND SOCIAL INFO
You can contact through davelagui@gmail.com.
In the web I am as @doanvelagui.
