#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <sys/nearptr.h>
#include <math.h>
#include "video.h"
#include "glib.h"
#include "bitmap.h"
#include "mouse.h"

#define matrix_size		3

// global options
int done = 0;

// interface options
int object = 0, tool = 0, option = -1;

// house
float housex[4] = {30, 60, 60, 30};
float housey[4] = {30, 30, 50, 50};
byte  housec = 9;
float roofx[3] = {25, 45, 65};
float roofy[3] = {30, 20, 30};
byte  roofc = 115;
float windowx[4] = {35, 40, 40, 35};
float windowy[4] = {35, 35, 40, 40};
byte  windowc = 11;
float doorx[4] = {45, 55, 55, 45};
float doory[4] = {35, 35, 50, 50};
byte  doorc = 114;

// tree
float stemx[4] = {90, 110, 107, 93};
float stemy[4] = {60, 60, 30, 30};
byte  stemc = 6;
float foliagex[11] = {80, 120, 110, 115, 105, 110, 100, 90, 95, 85, 90};
float foliagey[11] = {30,  30,  20,  20,  10,  10,   0, 10, 10, 20, 20};
byte  foliagec = 10;

// pivot
int pivot_x = 130, pivot_y = 100;

// button positions
int buttonxi[2] = {270, 295};
int buttonxf[2] = {290, 315};
int buttonyi[7] = { 5, 30, 55, 75,  95, 120, 140};
int buttonyf[7] = {20, 45, 70, 90, 110, 135, 155};

// button effects
BITMAP press, release;



// init variables
void init_vars(){
	load_bmp("PRESS.BMP", &press);
	load_bmp("RELEASE.BMP", &release);
}

// exit routine
void exit_routine(){
	done = 1;
}

// load interface
void load_interface(){
	BITMAP bmp;
	load_bmp("TPBAR.BMP", &bmp);
	draw_bitmap(&bmp, VGA, 265, 0);
	
	load_bmp("TSELE.BMP", &bmp);
	draw_bitmap(&bmp, VGA, 265, 25);
	draw_transparent_bitmap(&press, VGA, buttonxi[0], buttonyi[1]);
	
	load_bmp("TTOOL.BMP", &bmp);
	draw_bitmap(&bmp, VGA, 265, 50);
	draw_transparent_bitmap(&press, VGA, buttonxi[0], buttonyi[2]);
	
	load_bmp("TTRAN.BMP", &bmp);
	draw_bitmap(&bmp, VGA, 265, 115);
	
	load_bmp("TFREE.BMP", &bmp);
	draw_bitmap(&bmp, VGA, 265, 160);
}



// house
void draw_house(){
	fill_Polygonf(4, housex, housey, housec);
	fill_Polygonf(3, roofx, roofy, roofc);
	fill_Polygonf(4, windowx, windowy, windowc);
	fill_Polygonf(4, doorx, doory, doorc);
}

void erase_house(){
	int i, xmin = 320, xmax = 0, ymin = 200, ymax = 0;
	for (i = 0; i < 3; i++){
		if (roofx[i] < xmin)
			xmin = roofx[i];
		if (roofx[i] > xmax)
			xmax = roofx[i];
		
		if (roofy[i] < ymin)
			ymin = roofy[i];
		if (roofy[i] > ymax)
			ymax = roofy[i];
	}
	for (i = 0; i < 4; i++){
		if (housex[i] < xmin)
			xmin = housex[i];
		if (housex[i] > xmax)
			xmax = housex[i];
		if (windowx[i] < xmin)
			xmin = windowx[i];
		if (windowx[i] > xmax)
			xmax = windowx[i];
		if (doorx[i] < xmin)
			xmin = doorx[i];
		if (doorx[i] > xmax)
			xmax = doorx[i];
		
		if (housey[i] < ymin)
			ymin = housey[i];
		if (housey[i] > ymax)
			ymax = housey[i];
		if (windowy[i] < ymin)
			ymin = windowy[i];
		if (windowy[i] > ymax)
			ymax = windowy[i];
		if (doory[i] < ymin)
			ymin = doory[i];
		if (doory[i] > ymax)
			ymax = doory[i];
	}
	fill_Rectanglef(xmin, ymin, xmax+1, ymax+1, 0);
}



// tree
void draw_tree(){
	fill_Polygonf(4, stemx, stemy, stemc);
	fill_Polygonf(11, foliagex, foliagey, foliagec);
}

void erase_tree(){
	int i, xmin = 320, xmax = 0, ymin = 200, ymax = 0;
	for (i = 0; i < 4; i++){
		if (stemx[i] < xmin)
			xmin = stemx[i];
		if (stemx[i] > xmax)
			xmax = stemx[i];
		
		if (stemy[i] < ymin)
			ymin = stemy[i];
		if (stemy[i] > ymax)
			ymax = stemy[i];
	}
	
	for (i = 0; i < 11; i++){
		if (foliagex[i] < xmin)
			xmin = foliagex[i];
		if (foliagex[i] > xmax)
			xmax = foliagex[i];
		
		if (foliagey[i] < ymin)
			ymin = foliagey[i];
		if (foliagey[i] > ymax)
			ymax = foliagey[i];
	}
	fill_Rectanglef(xmin, ymin, xmax+1, ymax+1, 0);
}



// pointer
void draw_pivot(){
	draw_H_Linef(pivot_x - 5, pivot_x + 6, pivot_y, 15, 1);
	draw_V_Linef(pivot_x, pivot_y - 5, pivot_y + 6, 15, 1);
	draw_Circlef(pivot_x, pivot_y, 3, 15, 1);
}

void erase_pivot(){
	draw_H_Linef(pivot_x - 5, pivot_x + 6, pivot_y, 0, 1);
	draw_V_Linef(pivot_x, pivot_y - 5, pivot_y + 6, 0, 1);
	draw_Circlef(pivot_x, pivot_y, 3, 0, 1);
}



// matrix multiplication
void matrix_mult(float * matrix, float * vector, float * result){
	int i, j;
	for (i = 0; i < matrix_size; i++){
		result[i] = 0;
		for (j = 0; j < matrix_size; j++){
			result[i] += matrix[i*matrix_size+j]*vector[j];
		}
	}
}

// translate
void translate(int tx, int ty, int size, float* px, float* py){
	float trans[matrix_size*matrix_size] = {1, 0, tx, 0, 1, ty, 0, 0, 1};
	float vector[matrix_size], result[matrix_size];
	int i;
	for(i = 0; i < size; i++){
		vector[0] = px[i];
		vector[1] = py[i];
		vector[2] = 1;
		matrix_mult(trans, vector, result);
		px[i] = result[0];
		py[i] = result[1];
	}
}

// rotate
void rotate(float grados, int size, float* px, float* py){
	float trans[matrix_size*matrix_size] = {(float)cos((double)grados*PI/180), -(float)sin((double)grados*PI/180), 0, (float)sin((double)grados*PI/180), (float)cos((double)grados*PI/180), 0, 0, 0, 1};
	float vector[matrix_size], result[matrix_size];
	int i;
	for(i = 0; i < size; i++){
		vector[0] = px[i];
		vector[1] = py[i];
		vector[2] = 1;
		matrix_mult(trans, vector, result);
		px[i] = result[0];
		py[i] = result[1];
	}
}

// scale
void scale(float sx, float sy, int size, float* px, float* py){
	float trans[matrix_size*matrix_size] = {sx, 0, 0, 0, sy, 0, 0, 0, 1};
	float vector[matrix_size], result[matrix_size];
	int i;
	for(i = 0; i < size; i++){
		vector[0] = px[i];
		vector[1] = py[i];
		vector[2] = 1;
		matrix_mult(trans, vector, result);
		px[i] = result[0];
		py[i] = result[1];
	}
}

// shear
void shear(float sx, float sy, int size, float * px, float * py){
	float trans[matrix_size*matrix_size] = {1, sx, 0, sy, 1, 0, 0, 0, 1};
	float vector[matrix_size], result[matrix_size];
	int i;
	for(i = 0; i < size; i++){
		vector[0] = px[i];
		vector[1] = py[i];
		vector[2] = 1;
		matrix_mult(trans, vector, result);
		px[i] = result[0];
		py[i] = result[1];
	}
}

// flip
void flip(float fx, float fy, int size, float * px, float * py){
	float trans[matrix_size*matrix_size] = {fx, 0, 0, 0, fy, 0, 0, 0, 1};
	float vector[matrix_size], result[matrix_size];
	int i;
	for(i = 0; i < size; i++){
		vector[0] = px[i];
		vector[1] = py[i];
		vector[2] = 1;
		matrix_mult(trans, vector, result);
		px[i] = result[0];
		py[i] = result[1];
	}
}



// redraw pivot
int redraw_house(int x, int y){
	int i, xmin = 320, xmax = 0, ymin = 200, ymax = 0;
	for (i = 0; i < 3; i++){
		if (roofx[i] < xmin)
			xmin = roofx[i];
		if (roofx[i] > xmax)
			xmax = roofx[i];
		
		if (roofy[i] < ymin)
			ymin = roofy[i];
		if (roofy[i] > ymax)
			ymax = roofy[i];
	}
	for (i = 0; i < 4; i++){
		if (housex[i] < xmin)
			xmin = housex[i];
		if (housex[i] > xmax)
			xmax = housex[i];
		if (windowx[i] < xmin)
			xmin = windowx[i];
		if (windowx[i] > xmax)
			xmax = windowx[i];
		if (doorx[i] < xmin)
			xmin = doorx[i];
		if (doorx[i] > xmax)
			xmax = doorx[i];
		
		if (housey[i] < ymin)
			ymin = housey[i];
		if (housey[i] > ymax)
			ymax = housey[i];
		if (windowy[i] < ymin)
			ymin = windowy[i];
		if (windowy[i] > ymax)
			ymax = windowy[i];
		if (doory[i] < ymin)
			ymin = doory[i];
		if (doory[i] > ymax)
			ymax = doory[i];
	}
	if (pivot_x < xmax && pivot_x > xmin && pivot_y < ymax && pivot_y > ymin){
		fill_Rectanglef(xmin, ymin, xmax+1, ymax+1, 0);
		return 1;
	}
	if (x < xmax && x > xmin && y < ymax && y > ymin){
		return 1;
	}
	return 0;
}

int redraw_tree(int x, int y){
	int i, xmin = 320, xmax = 0, ymin = 200, ymax = 0;
	for (i = 0; i < 4; i++){
		if (stemx[i] < xmin)
			xmin = stemx[i];
		if (stemx[i] > xmax)
			xmax = stemx[i];
		
		if (stemy[i] < ymin)
			ymin = stemy[i];
		if (stemy[i] > ymax)
			ymax = stemy[i];
	}
	for (i = 0; i < 11; i++){
		if (foliagex[i] < xmin)
			xmin = foliagex[i];
		if (foliagex[i] > xmax)
			xmax = foliagex[i];
		
		if (foliagey[i] < ymin)
			ymin = foliagey[i];
		if (foliagey[i] > ymax)
			ymax = foliagey[i];
	}
	if (pivot_x < xmax && pivot_x > xmin && pivot_y < ymax && pivot_y > ymin){
		fill_Rectanglef(xmin, ymin, xmax+1, ymax+1, 0);
		return 1;
	}
	if (x < xmax && x > xmin && y < ymax && y > ymin){
		return 1;
	}
	return 0;
}



// manage tool buttons
void set_tool(int new_tool){
	if (tool != new_tool){
		BITMAP bmp;
		switch (tool){
			case 0:
				draw_transparent_bitmap(&release, VGA, buttonxi[0], buttonyi[2]);
				break;
			case 1:
				draw_transparent_bitmap(&release, VGA, buttonxi[1], buttonyi[2]);
				break;
			case 2:
				draw_transparent_bitmap(&release, VGA, buttonxi[0], buttonyi[3]);
				break;
			case 3:
				draw_transparent_bitmap(&release, VGA, buttonxi[1], buttonyi[3]);
				break;
			case 4:
				draw_transparent_bitmap(&release, VGA, buttonxi[0], buttonyi[4]);
				break;
		}
		switch (new_tool){
			case 0:
				draw_transparent_bitmap(&press, VGA, buttonxi[0], buttonyi[2]);
				load_bmp("TTRAN.BMP", &bmp);
				break;
			case 1:
				draw_transparent_bitmap(&press, VGA, buttonxi[1], buttonyi[2]);
				load_bmp("TSCAL.BMP", &bmp);
				break;
			case 2:
				draw_transparent_bitmap(&press, VGA, buttonxi[0], buttonyi[3]);
				load_bmp("TROTA.BMP", &bmp);
				break;
			case 3:
				draw_transparent_bitmap(&press, VGA, buttonxi[1], buttonyi[3]);
				load_bmp("TSHEA.BMP", &bmp);
				break;
			case 4:
				draw_transparent_bitmap(&press, VGA, buttonxi[0], buttonyi[4]);
				load_bmp("TFLIP.BMP", &bmp);
				break;
		}
		tool = new_tool;
		draw_bitmap(&bmp, VGA, 265, 115);
	}
}

void manage_tool(int opt){
	if (object){
		erase_tree();
		translate(-pivot_x, -pivot_y, 4, stemx, stemy);
		translate(-pivot_x, -pivot_y, 11, foliagex, foliagey);
	}else{
		erase_house();
		translate(-pivot_x, -pivot_y, 4, housex, housey);
		translate(-pivot_x, -pivot_y, 3, roofx, roofy);
		translate(-pivot_x, -pivot_y, 4, windowx, windowy);
		translate(-pivot_x, -pivot_y, 4, doorx, doory);
	}
	
	switch(tool){
		case 0:
			switch(opt){ // translation
				case 0:
					if (object){	// tree
						translate(0, -5, 4, stemx, stemy);
						translate(0, -5, 11, foliagex, foliagey);
					} else{			// house
						translate(0, -5, 4, housex, housey);
						translate(0, -5, 3, roofx, roofy);
						translate(0, -5, 4, windowx, windowy);
						translate(0, -5, 4, doorx, doory);
					}
					break;
				case 1:
					if (object){	// tree
						translate(0, 5, 4, stemx, stemy);
						translate(0, 5, 11, foliagex, foliagey);
					} else{			// house
						translate(0, 5, 4, housex, housey);
						translate(0, 5, 3, roofx, roofy);
						translate(0, 5, 4, windowx, windowy);
						translate(0, 5, 4, doorx, doory);
					}
					break;
				case 2:
					if (object){	// tree
						translate(-5, 0, 4, stemx, stemy);
						translate(-5, 0, 11, foliagex, foliagey);
					} else{			// house
						translate(-5, 0, 4, housex, housey);
						translate(-5, 0, 3, roofx, roofy);
						translate(-5, 0, 4, windowx, windowy);
						translate(-5, 0, 4, doorx, doory);
					}
					break;
				case 3:
					if (object){	// tree
						translate(5, 0, 4, stemx, stemy);
						translate(5, 0, 11, foliagex, foliagey);
					} else{			// house
						translate(5, 0, 4, housex, housey);
						translate(5, 0, 3, roofx, roofy);
						translate(5, 0, 4, windowx, windowy);
						translate(5, 0, 4, doorx, doory);
					}
					break;
			}
			break;
		case 1:
			switch(opt){ // escalation
				case 0:
					if (object){	// tree
						scale(1.1, 1, 4, stemx, stemy);
						scale(1.1, 1, 11, foliagex, foliagey);
					} else{			// house
						scale(1.1, 1, 4, housex, housey);
						scale(1.1, 1, 3, roofx, roofy);
						scale(1.1, 1, 4, windowx, windowy);
						scale(1.1, 1, 4, doorx, doory);
					}
					break;
				case 1:
					if (object){	// tree
						scale(0.909090909090909090909091, 1, 4, stemx, stemy);
						scale(0.909090909090909090909091, 1, 11, foliagex, foliagey);
					} else{			// house
						scale(0.909090909090909090909091, 1, 4, housex, housey);
						scale(0.909090909090909090909091, 1, 3, roofx, roofy);
						scale(0.909090909090909090909091, 1, 4, windowx, windowy);
						scale(0.909090909090909090909091, 1, 4, doorx, doory);
					}
					break;
				case 2:
					if (object){	// tree
						scale(1, 1.1, 4, stemx, stemy);
						scale(1, 1.1, 11, foliagex, foliagey);
					} else{			// house
						scale(1, 1.1, 4, housex, housey);
						scale(1, 1.1, 3, roofx, roofy);
						scale(1, 1.1, 4, windowx, windowy);
						scale(1, 1.1, 4, doorx, doory);
					}
					break;
				case 3:
					if (object){	// tree
						scale(1, 0.909090909090909090909091, 4, stemx, stemy);
						scale(1, 0.909090909090909090909091, 11, foliagex, foliagey);
					} else{			// house
						scale(1, 0.909090909090909090909091, 4, housex, housey);
						scale(1, 0.909090909090909090909091, 3, roofx, roofy);
						scale(1, 0.909090909090909090909091, 4, windowx, windowy);
						scale(1, 0.909090909090909090909091, 4, doorx, doory);
					}
					break;
			}
			break;
		case 2:
			switch(opt){ // rotation
				case 0:
					if (object){	// tree
						rotate(5, 4, stemx, stemy);
						rotate(5, 11, foliagex, foliagey);
					} else{			// house
						rotate(5, 4, housex, housey);
						rotate(5, 3, roofx, roofy);
						rotate(5, 4, windowx, windowy);
						rotate(5, 4, doorx, doory);
					}
					break;
				case 1:
					if (object){	// tree
						rotate(-5, 4, stemx, stemy);
						rotate(-5, 11, foliagex, foliagey);
					} else{			// house
						rotate(-5, 4, housex, housey);
						rotate(-5, 3, roofx, roofy);
						rotate(-5, 4, windowx, windowy);
						rotate(-5, 4, doorx, doory);
					}
					break;
			}
			break;
		case 3:
			switch(opt){ // shear
				case 0:
					if (object){	// tree
						shear(-0.1, 0, 4, stemx, stemy);
						shear(-0.1, 0, 11, foliagex, foliagey);
					} else{			// house
						shear(-0.1, 0, 4, housex, housey);
						shear(-0.1, 0, 3, roofx, roofy);
						shear(-0.1, 0, 4, windowx, windowy);
						shear(-0.1, 0, 4, doorx, doory);
					}
					break;
				case 1:
					if (object){	// tree
						shear(0.1, 0, 4, stemx, stemy);
						shear(0.1, 0, 11, foliagex, foliagey);
					} else{			// house
						shear(0.1, 0, 4, housex, housey);
						shear(0.1, 0, 3, roofx, roofy);
						shear(0.1, 0, 4, windowx, windowy);
						shear(0.1, 0, 4, doorx, doory);
					}
					break;
				case 2:
					if (object){	// tree
						shear(0, -0.1, 4, stemx, stemy);
						shear(0, -0.1, 11, foliagex, foliagey);
					} else{			// house
						shear(0, -0.1, 4, housex, housey);
						shear(0, -0.1, 3, roofx, roofy);
						shear(0, -0.1, 4, windowx, windowy);
						shear(0, -0.1, 4, doorx, doory);
					}
					break;
				case 3:
					if (object){	// tree
						shear(0, 0.1, 4, stemx, stemy);
						shear(0, 0.1, 11, foliagex, foliagey);
					} else{			// house
						shear(0, 0.1, 4, housex, housey);
						shear(0, 0.1, 3, roofx, roofy);
						shear(0, 0.1, 4, windowx, windowy);
						shear(0, 0.1, 4, doorx, doory);
					}
					break;
			}
			break;
		case 4:
			switch(opt){	// flip;
				case 0:
					if (object){	// tree
						flip(1, -1, 4, stemx, stemy);
						flip(1, -1, 11, foliagex, foliagey);
					} else{			// house
						flip(1, -1, 4, housex, housey);
						flip(1, -1, 3, roofx, roofy);
						flip(1, -1, 4, windowx, windowy);
						flip(1, -1, 4, doorx, doory);
					}
					break;
				case 1:
					if (object){	// tree
						flip(-1, 1, 4, stemx, stemy);
						flip(-1, 1, 11, foliagex, foliagey);
					} else{			// house
						flip(-1, 1, 4, housex, housey);
						flip(-1, 1, 3, roofx, roofy);
						flip(-1, 1, 4, windowx, windowy);
						flip(-1, 1, 4, doorx, doory);
					}
					break;
			}
			break;
	}
	
	if (object){	// tree
		translate(pivot_x, pivot_y, 4, stemx, stemy);
		translate(pivot_x, pivot_y, 11, foliagex, foliagey);
		draw_house();
		draw_tree();
	} else{			// house
		translate(pivot_x, pivot_y, 4, housex, housey);
		translate(pivot_x, pivot_y, 3, roofx, roofy);
		translate(pivot_x, pivot_y, 4, windowx, windowy);
		translate(pivot_x, pivot_y, 4, doorx, doory);
		draw_tree();
		draw_house();
	}
	draw_pivot();
}

// manage click
void manage_click(int x, int y){
	if (x < 265){
		erase_pivot();
		int rh = redraw_house(x, y);
		int rp = redraw_tree(x, y);
		pivot_x = x;
		pivot_y = y;
		if (rh || rp){
			draw_house();
			draw_tree();
		}
		draw_pivot();
	} else{
		if (x < buttonxf[0] && x > buttonxi[0]){
			if (y < buttonyf[1] && y > buttonyi[1]){ // select house
				if (object)
					draw_transparent_bitmap(&release, VGA, buttonxi[1], buttonyi[1]);
				object = 0;
				draw_transparent_bitmap(&press, VGA, buttonxi[0], buttonyi[1]);
			}
			
			else if (y < buttonyf[2] && y > buttonyi[2]){ // select translation
				set_tool(0);
			}
			else if (y < buttonyf[3] && y > buttonyi[3]){ // select rotation
				set_tool(2);
			}
			else if (y < buttonyf[4] && y > buttonyi[4]){ // select flip
				set_tool(4);
			}
			
			else if (y < buttonyf[5] && y > buttonyi[5]){ // option 0
				manage_tool(0);
			}
			else if (y < buttonyf[6] && y > buttonyi[6]){ // option 2
				if (!(tool == 2 && tool == 4))
					manage_tool(2);
			}
		}
		else if (x < buttonxf[1] && x > buttonxi[1]){
			if (y < buttonyf[0] && y > buttonyi[0]){ // exit
				exit_routine();
			}
			else if (y < buttonyf[1] && y > buttonyi[1]){ // select tree
				if (!object)
					draw_transparent_bitmap(&release, VGA, buttonxi[0], buttonyi[1]);
				object = 1;
				draw_transparent_bitmap(&press, VGA, buttonxi[1], buttonyi[1]);
			}
			
			else if (y < buttonyf[2] && y > buttonyi[2]){ // select escalation
				set_tool(1);
			}
			else if (y < buttonyf[3] && y > buttonyi[3]){ // select shear
				set_tool(3);
			}
			
			else if (y < buttonyf[5] && y > buttonyi[5]){ // option 1
				manage_tool(1);
			}
			else if (y < buttonyf[6] && y > buttonyi[6]){ // option 3
				if (!(tool == 2 && tool == 4))
					manage_tool(3);
			}
		}
	}
}



// main method
int main(){
	BITMAP bmp;
	MOUSE mouse;
	MOUSEBITMAP *mouse_bitmap;
	
	int x, y;
	word redraw, press, release;
	sword dx = 0, dy = 0, new_x, new_y;
	
	set_VGA_256();
	
	// Loading pointer
	if ((mouse_bitmap = (MOUSEBITMAP *)malloc(sizeof(MOUSEBITMAP))) == NULL){
		printf("Error allocating memory for pointer.\n");
		exit(1);
	}
	
	load_bmp("POINTER.BMP", &bmp);
	mouse.bmp = mouse_bitmap;
	if (!init_mouse(&mouse)){
		printf("Mouse not found.\n");
		exit(1);
	}
	
	for(y = 0; y < MOUSE_HEIGHT; y++){
		for(x = 0; x < MOUSE_WIDTH; x++){
			mouse_bitmap->data[x+y*MOUSE_WIDTH] = bmp.data[x+y*bmp.width];
		}
	}
	mouse_bitmap->hot_x = 0;
	mouse_bitmap->hot_y = 0;
	mouse_bitmap->next = NULL;
	
	free(bmp.data);
	
	new_x = mouse.x;
	new_y = mouse.y;
	redraw = 0xFFFF;
	
	// Loading graphic elements
	init_vars();
	load_interface();
	draw_house();
	draw_tree();
	draw_pivot();
	show_mouse(&mouse, VGA);
	
	// main cycle
	while (!done){
		if (redraw){
			wait_for_retrace();
			hide_mouse(&mouse, VGA);
			
			if (mouse.button1)
				manage_click(new_x, new_y);
			
			mouse.x = new_x;
			mouse.y = new_y;
			show_mouse(&mouse, VGA);
			redraw = 0;
		}
		
		do {
			get_mouse_motion(&dx, &dy);
			press    = get_mouse_press(LEFT_BUTTON);
			release  = get_mouse_release(LEFT_BUTTON);
		} while (dx==0 && dy==0 && press==0 && release==0);

		if (dx || dy) {
			new_x = mouse.x+dx;
			new_y = mouse.y+dy;
			if (new_x < 0)   new_x = 0;
			if (new_y < 0)   new_y = 0;
			if (new_x > 319) new_x = 319;
			if (new_y > 199) new_y = 199;
			redraw = 1;
		}
		if (press){
			mouse.button1 = 1;
			redraw = 1;
		}
		if (release){
			mouse.button1 = 0;
		}
	}
	free(mouse_bitmap);
	set_TEXT();
	return 0;
}

