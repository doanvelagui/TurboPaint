#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <sys/nearptr.h>
#include "video.h"

// 10h video interruption
void set_TEXT(){
    __asm__(
		"mov  $0x03, %ax;"	// TEXT mode
		"int  $0x10;"		// Video interruption
	);
	__djgpp_nearptr_disable();
}

// routine to set to VGA 256 colors video mode
void set_VGA_256() {
	if (__djgpp_nearptr_enable() == 0){
        printf("Can't access to first 640k of memory.\n");
        exit(-1);
    }
    __asm__(
		"mov  $0x13, %ax;"	// VGA 256 colours mode
		"int  $0x10;"		// Video interruption
	);
	VGA = (byte *)0xA0000 + __djgpp_conventional_base;
}

// put a pixel
void put_pixel(int x, int y, byte color){
	if(x > 79 && y > 24 && x < 320 && y < 200)
		VGA[(y << 8) + (y << 6) + x] = color;
	/*
	__asm__ (
		"movw	$0, %%cx;"		// clear cx
		"movb	%%bl, %%ch;"	// y * 256
		"shlw	$6, %%bx;"		// y * 64
		"addw	%%cx, %%bx;"	// y * 320
		"addw	%%ax, %%bx;"	// y * 320
		"movl	$0xA0000, %%eax;"	// VGA memory pointer
		"movb	%2, %%cl;"		// load color to dl
		"movb	%%cl, (%%eax, %%ebx);"	// Put pixel
		:	
		:	"a" (x), 		// mov x to eax
			"b" (y), 		// mov y to ebx
			"d" (color)		// mov color to edx
    );
	//*/
}

void put_pixelt(int x, int y, byte color){
	if(x >= 0 && y >= 0 && x < 265 && y < 200)
		VGA[(y << 8) + (y << 6) + x] = color;
}

// get color from pixel with (x, y) position on screen
byte get_color(int x, int y){
	return VGA[(y << 8) + (y << 6) + x];
	/*
	byte color;
	__asm__ (
		"movw	$0, %%cx;"		// clear cx
		"movb	%%bl, %%ch;"	// y * 256
		"shlw	$6, %%bx;"		// y * 64
		"addw	%%cx, %%bx;"	// y * 320
		"addw	%%ax, %%bx;"	// y * 320
		
		"movl	$0xA0000, %%eax;"	// VGA memory pointer
		"movb	(%%eax, %%ebx), %%dl;"	// get pixel
		"movb	%%dl, %0;"		// save pixel in color
		:	"=r" (color)
		:	"a" (x), 		// mov x to eax
			"b" (y) 		// mov y to ebx
    );
	return color;
	//*/
}

void super_patch(int id, byte color){
	int i, j, addr;
	if (id) addr = 42606;
	else addr = 42571;
	for (i = 13; i; i--){
		for(j = 24; j; j--){
			VGA[addr] = color;
			addr ++;
		}
		addr += 296;
	}
}
