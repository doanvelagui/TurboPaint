#define MOUSE_INT           0x33
#define MOUSE_RESET         0x00
#define MOUSE_GETPRESS      0x05
#define MOUSE_GETRELEASE    0x06
#define MOUSE_GETMOTION     0x0B
#define LEFT_BUTTON         0x00
#define RIGHT_BUTTON        0x01
#define MIDDLE_BUTTON       0x02

#define SCREEN_WIDTH		320
#define SCREEN_HEIGHT		200

#define MOUSE_WIDTH         12
#define MOUSE_HEIGHT        20
#define MOUSE_SIZE          (MOUSE_HEIGHT*MOUSE_WIDTH)

typedef unsigned char  byte;
typedef unsigned short word;
typedef unsigned long  dword;
typedef short sword;

typedef struct tagMOUSEBITMAP MOUSEBITMAP;
struct tagMOUSEBITMAP{
  int hot_x;
  int hot_y;
  byte data[MOUSE_SIZE];
  MOUSEBITMAP *next;   /* points to the next mouse bitmap, if any */
};

typedef struct{
	byte on;
	byte button1;
	byte button2;
	byte button3;
	int num_buttons;
	sword x;
	sword y;
	byte under[MOUSE_SIZE];
	MOUSEBITMAP *bmp;
} MOUSE;

void get_mouse_motion(sword *dx, sword *dy);
sword init_mouse(MOUSE *mouse);
extern sword get_mouse_press(sword button);
extern sword get_mouse_release(sword button);
extern void show_mouse(MOUSE *mouse, byte *VGA);
extern void hide_mouse(MOUSE *mouse, byte *VGA);

