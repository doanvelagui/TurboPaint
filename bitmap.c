#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <sys/nearptr.h>

#include "bitmap.h"

void file_skip(FILE *fp, int num_bytes){
	int i;
	for (i = num_bytes; i; i--)
		fgetc(fp);
}

void load_bmp(char *file, BITMAP *b){
	FILE *fp;
	long index;
	word num_colors;
	int x;

	// open the file
	if ((fp = fopen(file,"rb")) == NULL){
		printf("Error opening file %s.\n",file);
		exit(1);
	}

	// check to see if it is a valid bitmap file
	if (fgetc(fp)!='B' || fgetc(fp)!='M'){
		fclose(fp);
		printf("%s is not a bitmap file.\n",file);
		exit(1);
	}
	
	// skip DWord size, DWord reserved, DWord offset, DWord headerSize
	file_skip(fp,16);
	
	// read DWord width
	fread(&b->width, sizeof(word), 1, fp);
	file_skip(fp,2);
	
	// read DWord height
	fread(&b->height,sizeof(word), 1, fp);
	file_skip(fp,2);
	
	// skip word planes, word bitsPerPixel, DWord compression, DWord sizeImage
	// DWord xPixelPerMeter, DWord yPixelPerMeter
	file_skip(fp,20);
	
	// read DWord colorsUsed
	fread(&num_colors,sizeof(word), 1, fp);
	file_skip(fp,2);
	
	// skip DWord colorsImportant
	file_skip(fp,4);

	// assume we are working with an 8-bit file
	if (num_colors==0) num_colors=256;


	/* try to allocate memory */
	if ((b->data = (byte *) malloc((word)(b->width*b->height))) == NULL){
		fclose(fp);
		printf("Error allocating memory for file %s.\n",file);
		exit(1);
	}

	// read the palette information.
	for(index=0;index<num_colors;index++){
		// get RGB information
		b->palette[(int)(index*3+2)] = fgetc(fp) >> 2;
		b->palette[(int)(index*3+1)] = fgetc(fp) >> 2;
		b->palette[(int)(index*3+0)] = fgetc(fp) >> 2;
		x = fgetc(fp); // skip reserved 
	}

	// read the bitmap
	for(index=(b->height-1)*b->width;index>=0;index-=b->width)
		for(x=0;x<b->width;x++)
			b->data[(word)index+x]=(byte)fgetc(fp);

	fclose(fp);
}

void draw_bitmap(BITMAP *bmp, byte* VGA, int x, int y){
	int i, j;
	word screen_offset = (y << 8)+(y << 6) + x; // y*320 + x
	word bitmap_offset = 0;
	
	for(j=0; j < bmp->height; j++){
		for(i=0; i < bmp->width; i++, bitmap_offset++){
			VGA[screen_offset + i] = bmp->data[bitmap_offset];
		}
		screen_offset += 320; //screen width
	}
}

void draw_transparent_bitmap(BITMAP *bmp, byte* VGA, int x, int y){
	int i, j;
	word screen_offset = (y << 8)+(y << 6) + x; // y*320 + x
	word bitmap_offset = 0;
	byte data;
	
	for(j=0; j < bmp->height; j++){
		for(i=0; i < bmp->width; i++, bitmap_offset++){
			data = bmp->data[bitmap_offset];
			if (data) VGA[screen_offset + i] = data;
		}
		screen_offset += 320; //screen width
	}
}

void save_bitmap_from(byte* source, char *file, int x1, int y1, int x2, int y2){
	if (x2 < x1){
		int tmp = x1;
		x1 = x2;
		x2 = tmp;
	}
	if (y2 < y1){
		int tmp = y1;
		y1 = y2;
		y2 = tmp;
	}
	y1 --;
	y2 --;
	
	int dx = (x2 - x1), dy = (y2 - y1);
	FILE *fp;
	fp = fopen(file, "wb");
	
	// write "BM"
	char x[2] = "BM";
	fwrite(x, sizeof(x[0]), 2, fp);
	
	// writing rest of header
	dword toWrite[13];
	toWrite[0] = dx * dy + 1024 + 54;	// size: total pixels + 54 bytes header
	toWrite[1] = 0;						// reserved: set to 0
	toWrite[2] = 1024 + 54;				// offset: set to 54
	toWrite[3] = 40;					// header size: set to 40
	toWrite[4] = dx;					// width
	toWrite[5] = dy;					// height
	toWrite[6] = 0x80001;				// word planes: set to 1, word BPP: set to 8
	toWrite[7] = 0;						// compression: usually 0
	toWrite[8] = dx * dy;				// size of bitmap
	toWrite[9] = 0;						// HPPM: set to 0 
	toWrite[10] = 0;					// VPPM: set to 0
	toWrite[11] = 256;					// colours used
	toWrite[12] = 256;					// important colours
	
	fwrite(toWrite, sizeof(toWrite[0]), 13, fp);
	
	// Write colours palette information;
	dword palette[256];
	palette[0] = 0;
	palette[1] = 0x8;
	palette[2] = 0x800;
	palette[3] = 0x808;
	palette[4] = 0x80000;
	palette[5] = 0x80008;
	palette[6] = 0x80400;
	palette[7] = 0xC0C0C;
	palette[8] = 0x80808;
	palette[9] = 0xFF;
	palette[10] = 0xFF00;
	palette[11] = 0xFFFF;
	palette[12] = 0xFF0000;
	palette[13] = 0xFF00FF;
	palette[14] = 0xFFFF00;
	palette[15] = 0xFFFFFF;
	int i;
	for (i = 16; i<256; i++)
		palette[i] = 0;
	fwrite(palette, sizeof(dword), 256, fp);
	
	// Writing pixels
	int index = (y2 << 8) + (y2 << 6) + x1;
	for (i = dy; i; i--){
		fwrite(&source[index], sizeof(byte), dx, fp);
		index -= 320;
	}
	
	// close file
	fclose(fp);
}

void wait_for_retrace(void){
    /* wait until done with vertical retrace */
    while  ((inp(INPUT_STATUS) & VRETRACE));
    /* wait until done refreshing */
    while (!(inp(INPUT_STATUS) & VRETRACE));
}

