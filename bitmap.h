#define PALETTE_INDEX       0x03c8
#define PALETTE_DATA        0x03c9
#define INPUT_STATUS        0x03da
#define VRETRACE            0x08

typedef unsigned char  byte;
typedef unsigned short word;
typedef unsigned long  dword;

typedef struct tagBITMAP{
	word width;
	word height;
	byte palette[256*3];
	byte *data;
}	BITMAP;

extern void load_bmp(char *file, BITMAP *b);
extern void draw_bitmap(BITMAP *bmp, byte* VGA, int x, int y);
extern void draw_transparent_bitmap(BITMAP *bmp, byte* VGA, int x, int y);
extern void save_bitmap_from(byte* source, char *file, int x1, int y1, int x2, int y2);

extern void wait_for_retrace(void);
