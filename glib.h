typedef struct tagCLIPBOARD{
	int width;
	int height;
	byte *data;
}	CLIPBOARD;

extern void draw_Line(int x1, int y1, int x2, int y2, byte color, int thick);

extern void draw_Rectangle(int x1, int y1, int x2, int y2, byte color, int thick);
extern void fill_Rectangle(int x1, int y1, int x2, int y2, byte color);

extern void draw_Circle(int xc, int yc, int rad, byte color, int thick);
extern void fill_Circle(int xc, int yc, int rad, byte color);

extern void draw_Ellipse(int xc, int yc, int rx, int ry, byte color, int thick);
extern void fill_Ellipse(int xc, int yc, int rx, int ry, byte color);

extern void draw_Polygon(int size, int* x, int* y, byte color, int thick);
extern void fill_Polygon(int size, int* x, int* y, byte color);

extern void solid_Fill(int x, int y, byte color, byte oldColor);
extern void pattern1_Fill(int x, int y, byte color1, byte color2, byte oldColor, int i);
extern void pattern2_Fill(int x, int y, byte color1, byte color2, byte oldColor, int i, int j);

